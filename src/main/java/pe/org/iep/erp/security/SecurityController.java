package pe.org.iep.erp.security;

import java.security.Principal;
import javax.servlet.http.HttpServletRequest;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.albatross.zelpers.miscelanea.ExceptionHandler;
import pe.albatross.zelpers.miscelanea.JsonResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.org.iep.erp.model.seguridad.Menu;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.zelper.general.Constantine;
import pe.org.iep.erp.zelper.general.DataSession;

@Controller
public class SecurityController {

    @Autowired
    SecurityService service;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping("/")
    public String route66(HttpSession session) {

        DataSession usuario = (DataSession) session.getAttribute(Constantine.SESSION_USUARIO);

        if (usuario == null) {
            return "redirect:/login";
        }

        if (usuario.getUsuario().getClaveTemporal() != null && usuario.getUsuario().getClaveTemporal()) {
            return "redirect:/changepassword";
        }

        return "redirect:/dashboard";
    }

    @RequestMapping("login")
    public String loginPage(String error, HttpSession session, Model model) {

        DataSession usuario = (DataSession) session.getAttribute(Constantine.SESSION_USUARIO);

        if (usuario != null) {
            return "redirect:/";
        }

        if (error != null) {
            model.addAttribute("loginError", true);
        }

        return "security/login";
    }

    @RequestMapping(value = "changepassword", method = RequestMethod.GET)
    public String changePasswordPage(String error, HttpSession session, Model model) {
        return "security/changepassword";
    }

    @RequestMapping(value = "lagunas/{email:.*}", method = RequestMethod.GET)
    public String lagunas(@PathVariable String email, HttpSession session) {

        service.loginManually(email, session);
        return "redirect:/";
    }

    @RequestMapping("successLogin")
    public String successLogin(Principal principal, HttpServletRequest rq, HttpSession session) {

        service.onAuthenticationSuccess(principal.getName(), session);

        DataSession usuario = (DataSession) session.getAttribute(Constantine.SESSION_USUARIO);
        if (usuario.getUsuario().getClaveTemporal() != null && usuario.getUsuario().getClaveTemporal()) {
            return "redirect:/changepassword";
        }
        String ruta = null;
        if (!usuario.getMenu().isEmpty()) {
            for (Menu menu : usuario.getMenu()) {

                ruta = "redirect:" + menu.getMenus().get(0).getRuta();
                break;
            }
        }
        return ruta == null ? "redirect:/dashboard" : ruta;
    }

    @RequestMapping(value = "security/changeRol", method = RequestMethod.GET)
    public String changRol() {
        return "security/change";
    }

    @ResponseBody
    @RequestMapping(value = "security/changePass")
    public JsonResponse changePass(@RequestBody Usuario u, HttpSession session) {
        JsonResponse json = new JsonResponse();

        try {
            DataSession ds = (DataSession) session.getAttribute(Constantine.SESSION_USUARIO);
            service.changePassword(ds.getUsuario(), u.getClave());
            session.setAttribute(Constantine.SESSION_USUARIO, ds);
            json.setSuccess(true);
            json.setMessage("Contraseña actualizada");

        } catch (PhobosException e) {
            ExceptionHandler.handlePhobosEx(e, json);

        } finally {
            return json;
        }
    }
}
