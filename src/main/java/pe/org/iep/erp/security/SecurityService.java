package pe.org.iep.erp.security;

import javax.servlet.http.HttpSession;
import pe.org.iep.erp.model.seguridad.Usuario;

public interface SecurityService {

    void onAuthenticationSuccess(String username, HttpSession session);

    void loginManually(String email, HttpSession session);

    void changePassword(Usuario user, String newPass);
}
