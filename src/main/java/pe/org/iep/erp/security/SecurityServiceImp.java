package pe.org.iep.erp.security;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.albatross.zelpers.miscelanea.ObjectUtil;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.org.iep.erp.dao.general.PersonaDAO;
import pe.org.iep.erp.dao.seguridad.MenuRolDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioRolDAO;
import pe.org.iep.erp.model.enums.MenuTipoEnum;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Menu;
import pe.org.iep.erp.model.seguridad.MenuRol;
import pe.org.iep.erp.model.seguridad.Rol;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.model.seguridad.UsuarioRol;
import pe.org.iep.erp.zelper.general.Constantine;
import pe.org.iep.erp.zelper.general.DataSession;

@Service
@Transactional(readOnly = true)
public class SecurityServiceImp implements UserDetailsService, SecurityService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PersonaDAO personaDAO;

    @Autowired
    UsuarioDAO userDAO;

    @Autowired
    MenuRolDAO menuRolDAO;

    @Autowired
    UsuarioRolDAO usuarioRolDAO;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.debug("Nombre {}", username);

        Usuario user = userDAO.findForLogin(username);
        if (user == null) {
            logger.debug("Usuario no Encontrado");
            throw new UsernameNotFoundException("Usuario no encontrado");
        }

        logger.debug("Normal {}", username);

        List<GrantedAuthority> authorities = new ArrayList();
        authorities.add(new SimpleGrantedAuthority("ADM"));

        return new User(username, user.getClave(), authorities);
    }

    @Override
    public void onAuthenticationSuccess(String username, HttpSession session) {

        Usuario user = userDAO.findForLogin(username);
        Persona person = user.getPersona();

        DataSession ds = new DataSession();
        ds.setUsuario(user);
        ds.setPersona(person);
        ds.setCargo(person.getCargo());
        ds.setRoles(usuarioRolDAO.allByUsuario(user).stream().map(UsuarioRol::getRol).collect(Collectors.toList()));
        ds.setMenu(buildMenus(ds.getRoles()));
        session.setAttribute(Constantine.SESSION_USUARIO, ds);
    }

    @Override
    public void loginManually(String email, HttpSession session) {

        Usuario user = userDAO.findForLogin(email);

        if (user == null) {
            throw new PhobosException("Usuario no identificado.");
        }
        Persona persona = user.getPersona();

        List<GrantedAuthority> authorities = new ArrayList();
        authorities.add(new SimpleGrantedAuthority("ADM"));

        Authentication authentication = new UsernamePasswordAuthenticationToken(email, email, authorities);
        SecurityContext cntx = SecurityContextHolder.getContext();
        cntx.setAuthentication(authentication);

        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, cntx);

        DataSession ds = new DataSession();
        ds.setUsuario(user);
        ds.setPersona(persona);
        ds.setCargo(persona.getCargo());
        ds.setRoles(usuarioRolDAO.allByUsuario(user).stream().map(UsuarioRol::getRol).collect(Collectors.toList()));
        ds.setMenu(buildMenus(ds.getRoles()));

        session.setAttribute(Constantine.SESSION_USUARIO, ds);
    }

    @Override
    @Transactional
    public void changePassword(Usuario user, String newPass) {
        logger.debug("Nuevo password hashed: {}", newPass);
        user.setClave(newPass);
        user.setClaveTemporal(Boolean.FALSE);
        userDAO.update(user);
    }

    private List<Menu> buildMenus(List<Rol> roles) {
        List<Menu> menus = menuRolDAO.allByRoles(roles).stream().map(MenuRol::getMenu).distinct().collect(Collectors.toList());
//        Map<Menu, List<Menu>> menuSup = menus.stream().collect(Collectors.groupingBy(Menu::getMenuSuperior));
//        for (Map.Entry<Menu, List<Menu>> entry : menuSup.entrySet()) {
//            entry.getKey().setSubmenus(entry.getValue());
//        }
//
//        return menuSup.keySet().stream().sorted(Comparator.comparing(Menu::getOrden)).collect(Collectors.toList());
        return allMenuOrdered(menus);
    }

    public List<Menu> allMenuOrdered(List<Menu> menusBD) {
        List<Menu> menusMain = new ArrayList();
        Map<Long, Menu> mapMenusOrder = new LinkedHashMap();

        for (Menu menuBD : menusBD) {
            Menu menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior.menuSuperior.menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.TITULO, menusMain) != null) {
                continue;
            }

            menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior.menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.TITULO, menusMain) != null) {
                continue;
            }

            menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.TITULO, menusMain) != null) {
                continue;
            }

            crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.TITULO, menusMain);
        }

        for (Menu menuBD : menusBD) {
            Menu menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior.menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.MENU) != null) {
                continue;
            }

            menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior.menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.MENU_PADRE) != null) {
                continue;
            }

            menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.MENU) != null) {
                continue;
            }

            menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.MENU_PADRE) != null) {
                continue;
            }

            if (crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.MENU) != null) {
                continue;
            }

            crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.MENU_PADRE);
        }

        for (Menu menuBD : menusBD) {
            Menu menu = (Menu) ObjectUtil.getParentTree(menuBD, "menuSuperior");
            if (crearMenu(menu, mapMenusOrder, MenuTipoEnum.SUB_MENU) != null) {
                continue;
            }
            crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.SUB_MENU);
        }

        for (Menu menuBD : menusBD) {
            if (crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.OPCION) != null) {
                continue;
            }
            crearMenu(menuBD, mapMenusOrder, MenuTipoEnum.BOTON);
        }

        ordenarItems(menusMain, "");
        return menusMain;
    }

    private Menu crearMenu(Menu menu, Map<Long, Menu> mapMenusOrder, MenuTipoEnum tipoMenu, List<Menu> menusMain) {
        if (menu == null) {
            return menu;
        }

        if (menu.getTipoEnum() == tipoMenu) {
            Menu menuMap = mapMenusOrder.get(menu.getId());
            if (menuMap == null) {
                menu.setMenus(new ArrayList());
                mapMenusOrder.put(menu.getId(), menu);

                if (menusMain != null) {
                    menusMain.add(menu);
                }

                if (tipoMenu != MenuTipoEnum.TITULO) {
                    Menu parent = mapMenusOrder.get(menu.getMenuSuperior().getId());
                    parent.getMenus().add(menu);
                }
                return menu;
            }
        }
        return null;
    }

    private Menu crearMenu(Menu menu, Map<Long, Menu> mapMenusOrder, MenuTipoEnum tipoMenu) {
        return crearMenu(menu, mapMenusOrder, tipoMenu, null);
    }

    private void ordenarItems(List<Menu> menus, String tab) {
        Collections.sort(menus, new Menu.CompareOrden());
        menus.stream().forEachOrdered(menu -> {
            ordenarItems(menu.getMenus(), tab + "    ");
        });
    }
}
