package pe.org.iep.erp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "despliegue")
public class DespliegueConfig {

    @Value("${mailer}")
    Boolean mailer;

    @Value("${emails}")
    String emails;

    @Value("${copias}")
    String copias;

    @Value("${test}")
    Boolean test;

    public Boolean getMailer() {
        return mailer;
    }

    public void setMailer(Boolean mailer) {
        this.mailer = mailer;
    }

    public String getEmails() {
        return emails;
    }

    public void setEmails(String emails) {
        this.emails = emails;
    }

    public String getCopias() {
        return copias;
    }

    public void setCopias(String copias) {
        this.copias = copias;
    }

    public Boolean isTest() {
        return test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }

}
