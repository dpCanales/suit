package pe.org.iep.erp.config;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class AdviceConfig {

    @Autowired
    DespliegueConfig despliegueConfig;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ModelAttribute
    public void allModel(Model model) {
        model.addAttribute("testing", despliegueConfig.isTest());
    }

    @ExceptionHandler
    public String handleError(HttpServletRequest req, Exception ex) {
        
        logger.debug("URL MAPPING ERROR: {}", req.getRequestURL(), ex);
        
        return "redirect:/";
    }

}
