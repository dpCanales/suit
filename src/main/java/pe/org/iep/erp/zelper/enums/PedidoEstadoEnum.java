package pe.org.iep.erp.zelper.enums;

import java.util.HashMap;
import java.util.Map;

public enum PedidoEstadoEnum {

    DRAFT("Borrador"), APR("Aprobado"), FAB("Fabricando"), FIN("Finalizado"), ENT("Entregado"), ANU("Anulado");

    private final String value;
    private static final Map<String, PedidoEstadoEnum> lookup = new HashMap<>();

    static {
        for (PedidoEstadoEnum d : PedidoEstadoEnum.values()) {
            lookup.put(d.getValue(), d);
        }
    }

    private PedidoEstadoEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static PedidoEstadoEnum get(String abbreviation) {
        return lookup.get(abbreviation);
    }

    public static String getNombre(String nombre) {

        for (PedidoEstadoEnum d : PedidoEstadoEnum.values()) {
            if (d.name().equalsIgnoreCase(nombre)) {
                return d.getValue();
            }
        }
        return nombre;
    }
}
