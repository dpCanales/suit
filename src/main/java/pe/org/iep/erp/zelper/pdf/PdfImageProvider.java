package pe.org.iep.erp.zelper.pdf;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;
import com.itextpdf.tool.xml.pipeline.html.AbstractImageProvider;
import java.io.IOException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static pe.org.iep.erp.zelper.general.Constantine.PDF_IMG;

public class PdfImageProvider extends AbstractImageProvider {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Image retrieve(String src) {
        try {
            URL url = this.getClass().getResource(PDF_IMG + src);
            if (url == null) {
                url = new URL(src);
            }
            return Image.getInstance(url);

        } catch (BadElementException ex) {
            ex.getMessage();
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public String getImageRootPath() {
        return null;
    }
}
