package pe.org.iep.erp.zelper.general;

public interface Messages {

    String UPDATED = "Registro actualizado";

    String CREATED = "Registro agregado.";

    String DELETED = "Registro eliminado";

    String FK_ERROR = "Imposible eliminar, el registro está relacionado con otros registros.";

    String ERROR_GENERAL = "Esto es inoportuno pero se ha generado un problema desconocido. Por favor reporte al correo soporte@innovaschools.edu.pe.";

}
