package pe.org.iep.erp.zelper.pdf;

import com.google.common.base.Strings;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.engine.spi.TypedValue;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;
import pe.albatross.zelpers.miscelanea.PhobosException;
import static pe.org.iep.erp.zelper.general.Constantine.PDF_CSS;

@Component
public class PdfHtmlView extends AbstractPdfHtmlView {

    @Autowired
    private SpringTemplateEngine templateEngine;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document doc,
            PdfWriter writer, HttpServletRequest hsr, HttpServletResponse response) throws Exception {
        Context ctx = new Context();

        ctx.setVariables(model);

        PDFFormatoEnum dataEnum = (PDFFormatoEnum) model.get("formatoEnum");
        if (dataEnum == null) {
            throw new PhobosException("PDFFormatoEnum NO ESPECIFICADO.");
        }
        logger.debug("TYPE DOCUMENT GENERATE {}", dataEnum.getTitle());

        PdfContent pdfContent = new PdfContent();
        this.documentContent(pdfContent, dataEnum, ctx);
        this.documentProperty(doc, dataEnum);
        String resultado = "";
        String htmlContent = this.templateEngine.process(pdfContent.getTemplate(), pdfContent.getContext());

        HtmlCleaner cleaner = new HtmlCleaner();
        TagNode node = cleaner.clean(htmlContent);
        resultado = cleaner.getInnerHtml(node);

        HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
        htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());

//        PdfImageProvider imageProvider = new PdfImageProvider();
//        if (dataEnum == PDFFormatoEnum.FOTO_CHECK) {
//            int[] a = new int[1];
//            Image fox = imageProvider.retrieve("fotocheck.png");
//            fox.scaleAbsolute(200, 150);
//            Paragraph graph = new Paragraph();
//            graph.add("Nombre ");
//            graph.setAlignment(Paragraph.ALIGN_CENTER);
//            graph.add(fox);
//            doc.add(graph);
//        }
        htmlContext.setImageProvider(new PdfImageProvider());
        CSSResolver cssResolver = new StyleAttrCSSResolver();

        InputStream csspathtest = this.getClass().getResourceAsStream(PDF_CSS);

        CssFile cssfiletest = XMLWorkerHelper.getCSS(csspathtest);
        cssResolver.addCss(cssfiletest);
        Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, new HtmlPipeline(htmlContext, new PdfWriterPipeline(doc, writer)));
        XMLWorker worker = new XMLWorker(pipeline, true);
        XMLParser p = new XMLParser(worker);

        if (resultado != null) {
            p.parse(new StringReader(resultado));
        }

        String nombre = (String) model.get("nombrePdf");
        if (Strings.isNullOrEmpty(nombre)) {
            nombre = "untitle";
        }
        response.setHeader("Set-Cookie", "fileDownload=true; path=/");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + nombre + ".pdf\"");
    }

    private void image(Document document, PdfImageProvider imageProvider) throws DocumentException {

    }

    private void documentProperty(Document documentPdf, PDFFormatoEnum data) {

        documentPdf.setPageSize(PageSize.A4);
        documentPdf.addAuthor("Albatross");
        documentPdf.addCreationDate();
        documentPdf.addCreator("Albatross");
        documentPdf.addTitle(data.getTitle());
        documentPdf.addSubject(data.getSubject());

    }

    private void documentContent(PdfContent pdfContent, PDFFormatoEnum data, Context ctx) {

        pdfContent.setContext(ctx);
        pdfContent.setNombre(data.getName());
        pdfContent.setSubject(data.getSubject());
        pdfContent.setTitle(data.getTitle());
        pdfContent.setTemplate(data.getFileTemplate());

    }

}
