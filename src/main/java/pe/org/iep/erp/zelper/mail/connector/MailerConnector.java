package pe.org.iep.erp.zelper.mail.connector;

public interface MailerConnector {

    void sendMail(MailMessage mail);

    void sendMailSync(MailMessage mail);

}
