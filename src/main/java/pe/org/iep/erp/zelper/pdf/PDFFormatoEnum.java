package pe.org.iep.erp.zelper.pdf;

import java.util.HashMap;
import java.util.Map;

public enum PDFFormatoEnum {

    FOTO_CHECK("Fotocheck", "pdf/fotocheck", "Fotocheck", "Fotocheck");


    private final String name;
    private final String fileTemplate;
    private final String title;
    private final String subject;

    private static final Map<String, PDFFormatoEnum> lookup = new HashMap<>();

    static {
        for (PDFFormatoEnum d : PDFFormatoEnum.values()) {
            lookup.put(d.getName(), d);
        }
    }

    private PDFFormatoEnum(String name, String fileTemplate, String title, String subject) {
        this.name = name;
        this.fileTemplate = fileTemplate;
        this.title = title;
        this.subject = subject;
    }

    public static PDFFormatoEnum getEnum(String name) {
        for (PDFFormatoEnum d : PDFFormatoEnum.values()) {
            if (d.name().equalsIgnoreCase(name)) {
                return d;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public String getFileTemplate() {
        return fileTemplate;
    }

    public String getTitle() {
        return title;
    }

    public String getSubject() {
        return subject;
    }

}
