package pe.org.iep.erp.zelper.general;

import java.io.Serializable;
import java.util.List;
import pe.org.iep.erp.model.general.Suit.Cargo;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Menu;
import pe.org.iep.erp.model.seguridad.Rol;
import pe.org.iep.erp.model.seguridad.Usuario;

public class DataSession implements Serializable {

    private Persona persona;
    private Usuario usuario;
    private Cargo cargo;
    private List<Rol> roles;
    private List<Menu> menu;

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public List<Menu> getMenu() {
        return menu;
    }

    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

}
