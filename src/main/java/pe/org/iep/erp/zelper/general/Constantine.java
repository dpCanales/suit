package pe.org.iep.erp.zelper.general;

import pe.albatross.zelpers.miscelanea.OSValidator;

public interface Constantine {

    String SESSION_USUARIO = "SESSION_USUARIO_IEP";

    String TMP_DIR = OSValidator.isWindows() ? "C:/tmp/" : "/var/www/html/";

    String FILE_DIR = OSValidator.isWindows() ? "C:/iep-erp/" : "/tmp/";

    Integer DEFAULT_BUFFER_SIZE_DOWNLOAD = 1024;

    Integer MAX_IMG_SIZE = 300;

    Long ALMACEN_ID = 1L;

    Integer LOTE_SERIE = 1;

    Long EMPAQUETADORA_ID = 2L;

    String COD_MEDIA_DOCENA = "MDOC";

    String S3_BUCKET = "iep";

    String TEBAS_DIR = "iep-erp/";

    String S3_PRODUCTO_DIR = "productos/";

    String S3_PUBLIC_DIR = "publico/";

    String S3_LINK = "http://18.218.126.126/";

    Double IGV = 0.18;

    String CONTABILIDAD_DB = "dbo";

    Long TIPO_DOC_RUC = 4L;
    String PDF_CSS = "public/app/pdf/css/pdf.css";
    String PDF_IMG = "/public/app/pdf/img/";

    Long TIPO_DOC_OTRO = 1L;
    String AVATAR_MALE = "/phobos/images/avatar/male.png";
    String AVATAR_FEMALE = "/phobos/images/avatar/female.png";
    String AVATAR_UNKNOWN = "/phobos/images/avatar/unknown-person.gif";
    String AVATAR_MALE_PUBLIC = "male.png";
    String AVATAR_FEMALE_PUBLIC = "female.png";
    String AVATAR_UNKNOWN_PUBLIC = "unknown-person.gif";

}
