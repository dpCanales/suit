package pe.org.iep.erp.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum RolEnum {

    ADM("Administrado"), PER("Personal");

    private final String value;

    private static final Map<String, RolEnum> lookup = new HashMap<>();

    static {
        for (RolEnum d : RolEnum.values()) {
            lookup.put(d.getValue(), d);
        }
    }

    private RolEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static RolEnum get(String abbreviation) {
        return lookup.get(abbreviation);
    }

    public static String getNombre(String nombre) {

        for (RolEnum d : RolEnum.values()) {
            if (d.name().equalsIgnoreCase(nombre)) {
                return d.getValue();
            }
        }
        return nombre;
    }
}
