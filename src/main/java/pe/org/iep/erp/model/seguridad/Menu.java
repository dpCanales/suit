package pe.org.iep.erp.model.seguridad;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import pe.albatross.zelpers.miscelanea.TypesUtil;
import pe.org.iep.erp.model.enums.MenuTipoEnum;

@Entity
@Table(name = "menu")
public class Menu implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "icono")
    private String icono;

    @Column(name = "ruta")
    private String ruta;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "nivel")
    private Integer nivel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_menu_superior")
    private Menu menuSuperior;

    @OneToMany(mappedBy = "menuSuperior", fetch = FetchType.LAZY)
    private List<Menu> menus;

    @OneToMany(mappedBy = "menu", fetch = FetchType.LAZY)
    private List<MenuRol> menuRol;

    @Transient
    private List<Menu> submenus;

    public Menu() {
    }

    public Menu(Object id) {
        this.id = TypesUtil.getLong(id);
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

    public List<MenuRol> getMenuRol() {
        return menuRol;
    }

    public void setMenuRol(List<MenuRol> menuRol) {
        this.menuRol = menuRol;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Menu getMenuSuperior() {
        return menuSuperior;
    }

    public void setMenuSuperior(Menu menuSuperior) {
        this.menuSuperior = menuSuperior;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public List<Menu> getSubmenus() {
        return submenus;
    }

    public void setSubmenus(List<Menu> submenus) {
        this.submenus = submenus;
    }

    public MenuTipoEnum getTipoEnum() {
        if (tipo == null) {
            return null;
        }
        return MenuTipoEnum.valueOf(tipo);
    }

    public static class CompareOrden implements Comparator<Menu> {

        @Override
        public int compare(Menu menu1, Menu menu2) {
            return menu1.getOrden().compareTo(menu2.getOrden());
        }
    }
}
