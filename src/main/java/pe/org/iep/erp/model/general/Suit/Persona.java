package pe.org.iep.erp.model.general.Suit;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Strings;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.Years;
import pe.albatross.zelpers.miscelanea.TypesUtil;
import pe.org.iep.erp.model.enums.EstadoEnum;
import pe.org.iep.erp.model.seguridad.Rol;
import pe.org.iep.erp.zelper.general.Constantine;
import pe.org.iep.erp.zelper.serializador.DateTimeDeserializer;

@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre_completo")
    private String nombreCompleto;

    @Column(name = "nombres")
    private String nombres;

    @Column(name = "apellidos")
    private String apellidos;

    @Column(name = "numero_doc_identidad")
    private String numeroDocIdentidad;

    @Column(name = "email_personal")
    private String emailPersonal;

    @Column(name = "email_compania")
    private String emailCompania;

    @Column(name = "celular")
    private String celular;

    @Column(name = "ultimo_work")
    private String ultimoWork;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "hobby")
    private String hobby;

    @Column(name = "sexo")
    private String sexo;

    @Column(name = "ruta_foto")
    private String rutaFoto;

    @Column(name = "cursos")
    private String cursosConcat;

    @Column(name = "estado")
    private String estado;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "user_generado")
    private Boolean userGenerado;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_documento")
    private TipoDocIdentidad tipoDocumentoIdentidad;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "provincia_nacer")
    private Provincia provinciaNacer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cargo")
    private Cargo cargo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pais_nacer")
    private Pais paisNacer;

    @Column(name = "fecha_nacer")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacer;

    @Column(name = "fecha_ingreso")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Column(name = "fecha_retiro")
    @JsonDeserialize(using = DateTimeDeserializer.class)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRetiro;

    @OneToMany(mappedBy = "persona", fetch = FetchType.LAZY)
    private List<PersonaCurso> personasCargo;

    @Transient
    private List<Curso> cursos;

    @Transient
    private List<Rol> roles;

    public Persona() {
    }

    public Persona(Object id) {
        this.id = TypesUtil.getLong(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        if (apellidos != null && nombres != null) {

            this.nombreCompleto = apellidos.toUpperCase() + " " + nombres.toUpperCase();
        }
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getEmailPersonal() {
        return emailPersonal;
    }

    public void setEmailPersonal(String emailPersonal) {
        this.emailPersonal = emailPersonal;
    }

    public String getEmailCompania() {
        return emailCompania;
    }

    public void setEmailCompania(String emailCompania) {
        this.emailCompania = emailCompania;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getUltimoWork() {
        return ultimoWork;
    }

    public void setUltimoWork(String ultimoWork) {
        this.ultimoWork = ultimoWork;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public TipoDocIdentidad getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    public void setTipoDocumentoIdentidad(TipoDocIdentidad tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    public Provincia getProvinciaNacer() {
        return provinciaNacer;
    }

    public void setProvinciaNacer(Provincia provinciaNacer) {
        this.provinciaNacer = provinciaNacer;
    }

    public Date getFechaNacer() {
        return fechaNacer;
    }

    public void setFechaNacer(Date fechaNacer) {
        this.fechaNacer = fechaNacer;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public String getFullRutaFotoDocumento() {
        if (rutaFoto == null) {
            if (!Strings.isNullOrEmpty(this.sexo)) {
                switch (sexo) {
                    case "M":
                        return Constantine.AVATAR_MALE;
                    case "F":
                        return Constantine.AVATAR_FEMALE;
                }
            }
            return Constantine.AVATAR_UNKNOWN;
        }
        return rutaFoto;
    }

    public String getRutaFoto() {
        if (rutaFoto == null) {
            if (!Strings.isNullOrEmpty(this.sexo)) {
                switch (sexo) {
                    case "M":
                        return Constantine.AVATAR_MALE_PUBLIC;
                    case "F":
                        return Constantine.AVATAR_FEMALE_PUBLIC;
                }
            }
            return Constantine.AVATAR_UNKNOWN_PUBLIC;
        }
        return rutaFoto;
    }

    public void setRutaFoto(String rutaFoto) {
        this.rutaFoto = rutaFoto;
    }

    public Boolean getUserGenerado() {
        return userGenerado;
    }

    public void setUserGenerado(Boolean userGenerado) {
        this.userGenerado = userGenerado;
    }

    public Pais getPaisNacer() {
        return paisNacer;
    }

    public void setPaisNacer(Pais paisNacer) {
        this.paisNacer = paisNacer;
    }

    public List<PersonaCurso> getPersonasCargo() {
        return personasCargo;
    }

    public void setPersonasCargo(List<PersonaCurso> personasCargo) {
        this.personasCargo = personasCargo;
    }

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public String getCursosConcat() {
        return cursosConcat;
    }

    public void setCursosConcat(String cursosConcat) {
        this.cursosConcat = cursosConcat;
    }

    public String[] getListCursos() {

        return this.cursosConcat.split("/");
    }

    public Date getFechaRetiro() {
        return fechaRetiro;
    }

    public void setFechaRetiro(Date fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public EstadoEnum getEstadoEnum() {
        return EstadoEnum.valueOf(estado);
    }

    @JsonIgnore
    public void setEstadoEnum(EstadoEnum estado) {
        this.estado = estado.name();
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombreCompletoCapitalize() {

        return this.nombreCompleto.toLowerCase();
    }

    public Integer getEdad() {
        if (this.fechaNacer == null) {
            return null;
        }
        DateTime nac = new DateTime(this.fechaNacer);
        return Years.yearsBetween(nac, new DateTime()).getYears();
    }

    public String getAvatar() {

        String avatar = "";

        if (this.nombres != null) {
            avatar += this.nombres.substring(0, 1).toUpperCase();
        }

        if (this.apellidos != null) {
            avatar += this.apellidos.substring(0, 1).toUpperCase();
        }

        return avatar;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

}
