package pe.org.iep.erp.model.general.Suit;

import java.util.Date;
import javax.persistence.TemporalType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "read_excel")
public class ReadExcel implements Serializable {

    @Id
    @Column(name = "numero_doc_identidad")
    private String numeroDocIdentidad;

    @Column(name = "nombre_completo")
    private String nombreCompleto;

    @Column(name = "tipo_documento")
    private Long tipoDocumento;

    @Column(name = "cargo")
    private Integer cargo;

    @Column(name = "email_personal")
    private String emailPersonal;

    @Column(name = "email_compania")
    private String emailCompania;

    @Column(name = "celular")
    private String celular;

    @Column(name = "provincia_nacer")
    private String provinciaNacer;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;

    @Column(name = "fecha_nacer")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacer;

    @Column(name = "cursos")
    private String cursos;

    public ReadExcel() {
    }

    public String getNumeroDocIdentidad() {
        return numeroDocIdentidad;
    }

    public void setNumeroDocIdentidad(String numeroDocIdentidad) {
        this.numeroDocIdentidad = numeroDocIdentidad;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public Long getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(Long tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getCargo() {
        return cargo;
    }

    public void setCargo(Integer cargo) {
        this.cargo = cargo;
    }

    public String getEmailPersonal() {
        return emailPersonal;
    }

    public void setEmailPersonal(String emailPersonal) {
        this.emailPersonal = emailPersonal;
    }

    public String getEmailCompania() {
        return emailCompania;
    }

    public void setEmailCompania(String emailCompania) {
        this.emailCompania = emailCompania;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getProvinciaNacer() {
        return provinciaNacer;
    }

    public void setProvinciaNacer(String provinciaNacer) {
        this.provinciaNacer = provinciaNacer;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaNacer() {
        return fechaNacer;
    }

    public void setFechaNacer(Date fechaNacer) {
        this.fechaNacer = fechaNacer;
    }

    public String getCursos() {
        return cursos;
    }

    public void setCursos(String cursos) {
        this.cursos = cursos;
    }

}
