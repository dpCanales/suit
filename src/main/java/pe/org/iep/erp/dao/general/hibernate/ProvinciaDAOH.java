package pe.org.iep.erp.dao.general.hibernate;

import java.util.List;
import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.general.ProvinciaDAO;
import pe.org.iep.erp.model.general.Suit.Provincia;

@Repository
public class ProvinciaDAOH extends AbstractEasyDAO<Provincia> implements ProvinciaDAO {

    public ProvinciaDAOH() {
        super();
        setClazz(Provincia.class);
    }

    @Override
    public List<Provincia> allByNombre(String nombre) {
        nombre = "%" + nombre.replaceAll(" ", "%") + "%";
        Octavia sql = new Octavia()
                .from(Provincia.class, "pro")
                .filter("nombre", "like", nombre);

        return all(sql);

    }

}
