package pe.org.iep.erp.dao.general.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.general.CursoDAO;
import pe.org.iep.erp.model.general.Suit.Curso;

@Repository
public class CursoDAOH extends AbstractEasyDAO<Curso> implements CursoDAO {

    public CursoDAOH() {
        super();
        setClazz(Curso.class);
    }

    @Override
    public Curso findCodigo(String cursoString) {

        Octavia sql = new Octavia()
                .from(Curso.class, "cur")
                .filter("codigo", cursoString);

        return find(sql);
    }

}
