package pe.org.iep.erp.dao.general.hibernate;

import java.util.List;
import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.general.PaisDAO;
import pe.org.iep.erp.model.general.Suit.Pais;

@Repository
public class PaisDAOH extends AbstractEasyDAO<Pais> implements PaisDAO {

    public PaisDAOH() {
        super();
        setClazz(Pais.class);
    }

    @Override
    public List<Pais> allByName(String nombre) {
        nombre = "%" + nombre.replaceAll(" ", "%") + "%";
        Octavia sql = new Octavia()
                .from(Pais.class, "p")
                .filter("nombre", "like", nombre)
                .limit(15);

        return all(sql);
    }

}
