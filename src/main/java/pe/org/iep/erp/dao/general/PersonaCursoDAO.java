package pe.org.iep.erp.dao.general;

import java.util.List;
import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.PersonaCurso;

public interface PersonaCursoDAO extends EasyDAO<PersonaCurso> {

    public List<PersonaCurso> allByPersonas(List<Persona> personas);

    public List<PersonaCurso> findByPersona(Persona persona);

}
