package pe.org.iep.erp.dao.seguridad;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.model.seguridad.UsuarioRol;

public interface UsuarioRolDAO extends EasyDAO<UsuarioRol> {

    List<UsuarioRol> allByUsuarios(List<Usuario> usuarios);
    
    List<UsuarioRol> allByUsuario(Usuario usuario);
}
