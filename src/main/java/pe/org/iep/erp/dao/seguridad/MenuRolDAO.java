package pe.org.iep.erp.dao.seguridad;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.org.iep.erp.model.seguridad.MenuRol;
import pe.org.iep.erp.model.seguridad.Rol;

public interface MenuRolDAO extends EasyDAO<MenuRol> {

    public List<MenuRol> allByRol(Rol rol);

    public List<MenuRol> allByRoles(List<Rol> roles);

}
