package pe.org.iep.erp.dao.seguridad.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.albatross.octavia.dynatable.DynatableSql;
import pe.org.iep.erp.dao.seguridad.UsuarioDAO;
import static pe.org.iep.erp.model.enums.EstadoEnum.ACT;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Usuario;

@Repository
public class UsuarioDAOH extends AbstractEasyDAO<Usuario> implements UsuarioDAO {

    public UsuarioDAOH() {
        super();
        setClazz(Usuario.class);
    }

    @Override
    public Usuario findForLogin(String userName) {

        Octavia octavia = Octavia.query(Usuario.class, "u")
                .join("persona pe")
                .left("pe.cargo")
                .filter("u.usuario", userName)
                .filter("estado", "ACT");

        return this.find(octavia);
    }

    @Override
    public List<Usuario> allByDynatable(DynatableFilter filter) {
        DynatableSql sql = new DynatableSql(filter)
                .from(Usuario.class, "u")
                .join("persona per")
                .filter("id", "!=", 1)
                .searchFields("u.usuario")
                .searchFields("per.nombres", "per.email", "per.numeroDocIdentidad")
                .searchComplexField("concat(coalesce(per.paterno,''),' ',coalesce(per.materno,''),' ',coalesce(per.nombres,''))")
                .searchComplexField("concat(coalesce(per.nombres,''),' ',coalesce(per.paterno,''),' ',coalesce(per.materno,''))");

        return all(sql);
    }

    @Override
    public Usuario findByPersona(Persona personaForm) {
        Octavia octavia = Octavia.query(Usuario.class, "u")
                .join("persona pe")
                .left("pe.cargo")
                .filter("pe.id", personaForm)
                .filter("estado", ACT);

        return this.find(octavia);
    }
}
