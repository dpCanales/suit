package pe.org.iep.erp.dao.general;

import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.ReadExcel;

public interface ReadExcelDAO extends EasyDAO<ReadExcel> {

    public int generar();

    public int procesarCsv(String file);

    public void deleteAll();

}
