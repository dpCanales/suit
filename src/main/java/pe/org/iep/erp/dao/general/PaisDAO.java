package pe.org.iep.erp.dao.general;

import java.util.List;
import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.Pais;

public interface PaisDAO extends EasyDAO<Pais> {

    public List<Pais> allByName(String nombre);

}
