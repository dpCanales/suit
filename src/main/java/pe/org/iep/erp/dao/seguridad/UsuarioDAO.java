package pe.org.iep.erp.dao.seguridad;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Usuario;

public interface UsuarioDAO extends EasyDAO<Usuario> {

    Usuario findForLogin(String userName);

    public List<Usuario> allByDynatable(DynatableFilter filter);

    public Usuario findByPersona(Persona personaForm);

}
