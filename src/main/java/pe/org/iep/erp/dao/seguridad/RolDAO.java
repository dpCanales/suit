package pe.org.iep.erp.dao.seguridad;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.org.iep.erp.model.enums.RolEnum;
import pe.org.iep.erp.model.seguridad.Rol;

public interface RolDAO extends EasyDAO<Rol> {

    public List<Rol> allNoAdmin();
    
    public List<Rol> allByDynatable(DynatableFilter filter);

    public Rol findByCodigo(RolEnum rolEnum);

}
