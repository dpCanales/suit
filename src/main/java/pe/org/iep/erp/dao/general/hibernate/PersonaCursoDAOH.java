package pe.org.iep.erp.dao.general.hibernate;

import java.util.List;
import java.util.stream.Collectors;
import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.general.PersonaCursoDAO;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.PersonaCurso;

@Repository
public class PersonaCursoDAOH extends AbstractEasyDAO<PersonaCurso> implements PersonaCursoDAO {

    public PersonaCursoDAOH() {
        super();
        setClazz(PersonaCurso.class);
    }

    @Override
    public List<PersonaCurso> allByPersonas(List<Persona> personas) {
        List<Long> ids = personas.stream().map(x -> x.getId()).collect(Collectors.toList());

        Octavia sql = new Octavia()
                .from(PersonaCurso.class, "pc")
                .join("persona per", "curso cur")
                .in("per.id", ids);

        return all(sql);
    }

    @Override
    public List<PersonaCurso> findByPersona(Persona persona) {
        Octavia sql = new Octavia()
                .from(PersonaCurso.class, "pc")
                .join("persona per", "curso cur")
                .filter("per.id", persona);

        return all(sql);
    }

}
