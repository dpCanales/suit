package pe.org.iep.erp.dao.seguridad.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.albatross.octavia.dynatable.DynatableSql;
import pe.org.iep.erp.dao.seguridad.RolDAO;
import pe.org.iep.erp.model.enums.RolEnum;
import static pe.org.iep.erp.model.enums.RolEnum.ADM;
import pe.org.iep.erp.model.seguridad.Rol;

@Repository
public class RolDAOH extends AbstractEasyDAO<Rol> implements RolDAO {

    public RolDAOH() {
        super();
        setClazz(Rol.class);
    }

    @Override
    public List<Rol> allByDynatable(DynatableFilter filter) {
        DynatableSql sql = new DynatableSql(filter)
                .from(Rol.class, "r")
                .searchFields("r.codigo", "r.nombre")
                .orderBy("r.id desc");

        return all(sql);
    }

    @Override
    public List<Rol> allNoAdmin() {
        Octavia sql = Octavia.query(Rol.class, "r")
                .filter("r.codigo", "!=", ADM);

        return all(sql);
    }

    @Override
    public Rol findByCodigo(RolEnum rolEnum) {
        Octavia sql = Octavia.query(Rol.class, "r")
                .filter("r.codigo", rolEnum);

        return find(sql);
    }
}
