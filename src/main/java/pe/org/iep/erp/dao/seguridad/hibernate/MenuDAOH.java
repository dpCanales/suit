package pe.org.iep.erp.dao.seguridad.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.seguridad.MenuDAO;
import pe.org.iep.erp.model.seguridad.Menu;

@Repository
public class MenuDAOH extends AbstractEasyDAO<Menu> implements MenuDAO {

    public MenuDAOH() {
        super();
        setClazz(Menu.class);
    }

    @Override
    public List<Menu> allByTipo(String tipo) {
        Octavia sql = Octavia.query(Menu.class, "menu")
                .left("menu.menuSuperior ms")
                .orderBy("ms.id", "menu.orden")
                .filter("tipo",tipo);
        
        return all(sql);
    }
}
