package pe.org.iep.erp.dao.general;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;

public interface PersonaDAO extends EasyDAO<Persona> {

    List<Persona> allByNombre(String nombre);

    List<Persona> allByDocumento(TipoDocIdentidad tipo, String nroDoc);

    List<Persona> allByNombreDocumento(String nombre);

    List<Persona> allByDocumentoAndNotPersona(TipoDocIdentidad tipoDocumentoIdentidad, String numeroDocIdentidad, Persona personaDb);

    public Persona find(Persona persona);

    public List<Persona> allSinUser();

    public List<Persona> allByDynatable(DynatableFilter filter);

    public Persona findByTipoDoc(Persona personaForm);

}
