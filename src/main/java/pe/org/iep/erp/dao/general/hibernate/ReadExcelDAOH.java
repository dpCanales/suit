package pe.org.iep.erp.dao.general.hibernate;

import org.hibernate.SQLQuery;
import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.org.iep.erp.dao.general.ReadExcelDAO;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.ReadExcel;

@Repository
public class ReadExcelDAOH extends AbstractEasyDAO<ReadExcel> implements ReadExcelDAO {

    public ReadExcelDAOH() {
        super();
        setClazz(ReadExcel.class);
    }

    @Override
    public int generar() {
        StringBuilder strb = new StringBuilder("");
        strb.append(" INSERT into persona  ");
        strb.append(" SELECT  ");
        strb.append(" null, ");
        strb.append(" re.nombre_completo, ");
        strb.append(" SUBSTRING_INDEX(re.nombre_completo, ',', 1), ");
        strb.append(" SUBSTRING_INDEX(re.nombre_completo, ',', -1), ");
        strb.append(" re.numero_doc_identidad, ");
        strb.append(" tdi.id, ");
        strb.append(" re.email_compania, ");
        strb.append(" re.email_compania, ");
        strb.append(" re.celular, ");
        strb.append(" null, ");
        strb.append(" IF(re.fecha_nacer = '', null,STR_TO_DATE(re.fecha_nacer,'%Y-%m-%d')), ");
        strb.append(" pr.id, ");
        strb.append(" re.direccion, ");
        strb.append(" IF(re.fecha_ingreso = '',null, STR_TO_DATE(re.fecha_ingreso,'%Y-%m-%d')), ");
        strb.append(" null, ");
        strb.append(" null, ");
        strb.append(" null, ");
        strb.append(" ca.id, ");
        strb.append(" 0, ");
        strb.append(" null, ");
        strb.append(" null, ");
        strb.append(" re.cursos, ");
        strb.append(" null, ");
        strb.append(" 'ACT' ");
        strb.append(" FROM ");
        strb.append(" read_excel re ");
        strb.append(" LEFT JOIN tipo_doc_identidad tdi ");
        strb.append(" on re.tipo_documento = tdi.simbolo ");
        strb.append(" LEFT join provincia pr ");
        strb.append(" on re.provincia_nacer = pr.nombre  ");
        strb.append(" left join cargo ca ");
        strb.append(" on re.cargo = ca.nombre ");
        strb.append(" where not EXISTS (SELECT * from persona where numero_doc_identidad = re.numero_doc_identidad) ");

        SQLQuery query = getCurrentSession().createSQLQuery(strb.toString());
        query.addEntity(Persona.class);
        return query.executeUpdate();

    }

    @Override
    public int procesarCsv(String file) {
        System.out.println("Ruta_: " + file);
//        file = "/home/alb/Descargas/usuario.csv";
        String str = " LOAD DATA LOCAL INFILE '" + file + "'  INTO TABLE read_excel CHARACTER SET UTF8 FIELDS TERMINATED BY ';' ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n'  IGNORE 1 LINES ";

        SQLQuery query = getCurrentSession().createSQLQuery(str);
        query.addEntity(ReadExcel.class);
        return query.executeUpdate();
    }

    @Override
    public void deleteAll() {
        String str = "DELETE FROM read_excel  ";

        SQLQuery query = getCurrentSession().createSQLQuery(str);
        query.addEntity(ReadExcel.class);
        query.executeUpdate();
    }

}
