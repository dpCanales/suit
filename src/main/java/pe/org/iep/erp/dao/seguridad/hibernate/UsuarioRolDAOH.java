package pe.org.iep.erp.dao.seguridad.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.seguridad.UsuarioRolDAO;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.model.seguridad.UsuarioRol;

@Repository
public class UsuarioRolDAOH extends AbstractEasyDAO<UsuarioRol> implements UsuarioRolDAO {

    public UsuarioRolDAOH() {
        super();
        setClazz(UsuarioRol.class);
    }

    @Override
    public List<UsuarioRol> allByUsuario(Usuario usuario) {
        Octavia sql = Octavia.query(UsuarioRol.class, "ur")
                .join("ur.usuario u", "ur.rol r")
                .filter("u.id", usuario);
        
        return all(sql);
    }

    @Override
    public List<UsuarioRol> allByUsuarios(List<Usuario> usuarios) {
         Octavia sql = Octavia.query(UsuarioRol.class, "ur")
                .join("ur.usuario u", "ur.rol r")
                .in("u.id", usuarios);
        
        return all(sql);
    }
}
