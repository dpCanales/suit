package pe.org.iep.erp.dao.general;

import java.util.List;
import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.Provincia;

public interface ProvinciaDAO extends EasyDAO<Provincia> {

    public List<Provincia> allByNombre(String nombre);

}
