package pe.org.iep.erp.dao.general.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.org.iep.erp.dao.general.TipoDocumentoDAO;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;

@Repository
public class TipoDocumentoDAOH extends AbstractEasyDAO<TipoDocIdentidad> implements TipoDocumentoDAO {

    public TipoDocumentoDAOH() {
        super();
        setClazz(TipoDocIdentidad.class);
    }

}
