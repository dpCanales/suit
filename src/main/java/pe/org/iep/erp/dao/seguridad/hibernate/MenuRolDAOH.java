package pe.org.iep.erp.dao.seguridad.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.org.iep.erp.dao.seguridad.MenuRolDAO;
import pe.org.iep.erp.model.seguridad.MenuRol;
import pe.org.iep.erp.model.seguridad.Rol;

@Repository
public class MenuRolDAOH extends AbstractEasyDAO<MenuRol> implements MenuRolDAO {

    public MenuRolDAOH() {
        super();
        setClazz(MenuRol.class);
    }

    @Override
    public List<MenuRol> allByRol(Rol rol) {
        Octavia sql = Octavia.query(MenuRol.class, "mr")
                .join("menu m", "rol r")
                .filter("r.id", rol);
        
        return all(sql);
    }

    @Override
    public List<MenuRol> allByRoles(List<Rol> roles) {
         Octavia sql = Octavia.query(MenuRol.class, "mr")
                .join("menu m", "rol r")
                .in("r.id", roles);
        
        return all(sql);
    }
}
