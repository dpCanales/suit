package pe.org.iep.erp.dao.general;

import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.Cargo;

public interface CargoDAO extends EasyDAO<Cargo> {


}
