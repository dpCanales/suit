package pe.org.iep.erp.dao.general;

import pe.albatross.octavia.easydao.EasyDAO;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;

public interface TipoDocumentoDAO extends EasyDAO<TipoDocIdentidad> {


}
