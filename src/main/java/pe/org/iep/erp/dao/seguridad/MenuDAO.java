package pe.org.iep.erp.dao.seguridad;

import pe.albatross.octavia.easydao.EasyDAO;
import java.util.List;
import pe.org.iep.erp.model.seguridad.Menu;

public interface MenuDAO extends EasyDAO<Menu> {

    public List<Menu> allByTipo(String menu);

}
