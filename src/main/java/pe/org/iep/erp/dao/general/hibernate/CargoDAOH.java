package pe.org.iep.erp.dao.general.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import org.springframework.stereotype.Repository;
import pe.org.iep.erp.dao.general.CargoDAO;
import pe.org.iep.erp.model.general.Suit.Cargo;

@Repository
public class CargoDAOH extends AbstractEasyDAO<Cargo> implements CargoDAO {

    public CargoDAOH() {
        super();
        setClazz(Cargo.class);
    }

}
