package pe.org.iep.erp.dao.general.hibernate;

import pe.albatross.octavia.easydao.AbstractEasyDAO;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Repository;
import pe.albatross.octavia.Octavia;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.albatross.octavia.dynatable.DynatableSql;
import pe.org.iep.erp.dao.general.PersonaDAO;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;

@Repository
public class PersonaDAOH extends AbstractEasyDAO<Persona> implements PersonaDAO {

    public PersonaDAOH() {
        super();
        setClazz(Persona.class);
    }

    @Override
    public List<Persona> allByNombre(String nombre) {
        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .join("tipoDocumentoIdentidad")
                .beginBlock()
                .__().complexFilter("concat(coalesce(per.paterno,''),' ',coalesce(per.materno,''),' ',coalesce(per.nombres,''))", "like", nombre)
                .__().complexFilter("concat(coalesce(per.nombres,''),' ',coalesce(per.paterno,''),' ',coalesce(per.materno,''))", "like", nombre)
                .__().filter("numeroDocIdentidad", "like", nombre)
                .endBlock()
                .isNull("personaJuridica")
                .isNotNull("numeroDocIdentidad")
                .limit(15);

        return sql.all(getCurrentSession());
    }

    @Override
    public List<Persona> allByDocumento(TipoDocIdentidad tipo, String nroDoc) {
        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .join("tipoDocumentoIdentidad td")
                .filter("tipoDocumentoIdentidad", tipo)
                .filter("numeroDocIdentidad", nroDoc);

        return sql.all(getCurrentSession());
    }

    @Override
    public List<Persona> allByNombreDocumento(String nombre) {
        Octavia sql = Octavia.query(Persona.class, "per")
                .join("tipoDocumentoIdentidad")
                .beginBlock()
                .__().complexFilter("concat(coalesce(per.paterno,''),' ',coalesce(per.materno,''),' ',coalesce(per.nombres,''))", "like", nombre)
                .__().complexFilter("concat(coalesce(per.nombres,''),' ',coalesce(per.paterno,''),' ',coalesce(per.materno,''))", "like", nombre)
                .__().filter("per.numeroDocIdentidad", "like", nombre)
                .endBlock()
                .limit(15);
        return all(sql);
    }

    @Override
    public List<Persona> allByDocumentoAndNotPersona(TipoDocIdentidad tipoDocumentoIdentidad, String numeroDocIdentidad, Persona personaDb) {
        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .join("tipoDocumentoIdentidad td")
                .filter("td.id", tipoDocumentoIdentidad)
                .filter("per.numeroDocIdentidad", numeroDocIdentidad)
                .filter("per.id", "<>", personaDb);
        return sql.all(getCurrentSession());
    }

    @Override
    public Persona find(Persona persona) {
        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .left("tipoDocumentoIdentidad td")
                .left("provinciaNacer prn")
                .left("paisNacer pn")
                .left("cargo ca")
                .filter("per.id", persona);
        return find(sql);
    }

    @Override
    public List<Persona> allSinUser() {

        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .filter("userGenerado", "0");
        return all(sql);
    }

    @Override
    public List<Persona> allByDynatable(DynatableFilter filter) {
        DynatableSql sql = new DynatableSql(filter)
                .from(Persona.class, "per")
                .join("tipoDocumentoIdentidad td", "cargo car")
                .searchFields("td.simbolo", "per.numeroDocIdentidad", "per.celular", "per.emailCompania", "per.nombreCompleto")
                .searchFields("per.nombres", "per.apellidos");

        sql.beginRelativeFilters();
        setCondicionModalidad(filter, sql);
        return all(sql);
    }

    private void setCondicionModalidad(DynatableFilter filter, DynatableSql sql) {
        Map<String, Object> queries = filter.getQueries();
        if (queries == null) {
            return;
        }

        for (String key : queries.keySet()) {
            if (!key.equals("cargo")) {
                continue;
            }
            String values = (String) queries.get(key);
            sql.filter("car.id", values);

        }

    }

    @Override
    public Persona findByTipoDoc(Persona personaForm) {
        Octavia sql = Octavia.query()
                .from(Persona.class, "per")
                .left("tipoDocumentoIdentidad td")
                .left("provinciaNacer prn")
                .left("paisNacer pn")
                .left("cargo ca")
                .filter("td.id", personaForm.getTipoDocumentoIdentidad())
                .filter("numeroDocIdentidad", personaForm.getNumeroDocIdentidad());
        return find(sql);
    }

}
