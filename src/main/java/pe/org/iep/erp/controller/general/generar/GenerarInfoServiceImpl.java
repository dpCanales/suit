package pe.org.iep.erp.controller.general.generar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import pe.albatross.zelpers.file.system.FileHelper;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.albatross.zelpers.miscelanea.TypesUtil;
import pe.org.iep.erp.dao.general.CursoDAO;
import pe.org.iep.erp.dao.general.PersonaCursoDAO;
import pe.org.iep.erp.dao.general.PersonaDAO;
import pe.org.iep.erp.dao.general.ReadExcelDAO;
import pe.org.iep.erp.dao.seguridad.RolDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioRolDAO;
import pe.org.iep.erp.model.enums.EstadoEnum;
import static pe.org.iep.erp.model.enums.EstadoEnum.ACT;
import static pe.org.iep.erp.model.enums.RolEnum.PER;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.PersonaCurso;
import pe.org.iep.erp.model.seguridad.Rol;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.model.seguridad.UsuarioRol;
import static pe.org.iep.erp.zelper.general.Constantine.FILE_DIR;
import static pe.org.iep.erp.zelper.general.Constantine.S3_LINK;
import static pe.org.iep.erp.zelper.general.Constantine.TMP_DIR;

@Service
@Transactional
public class GenerarInfoServiceImpl implements GenerarInfoService {

    @Autowired
    ReadExcelDAO readExcelDAO;

    @Autowired
    PersonaDAO personaDAO;

    @Autowired
    UsuarioDAO usuarioDAO;

    @Autowired
    CursoDAO cursoDAO;

    @Autowired
    RolDAO rolDAO;

    @Autowired
    UsuarioRolDAO usuarioRolDAO;

    @Autowired
    PersonaCursoDAO personaCursoDAO;

    @Override
    @Transactional
    public void generar() {
        readExcelDAO.generar();
    }

    @Override
    @Transactional
    public void generarUsuario() {

        List<Persona> personas = personaDAO.allSinUser();
        Rol rol = rolDAO.findByCodigo(PER);
        for (Persona persona : personas) {
            String encodedString = DigestUtils.md5Hex(persona.getNumeroDocIdentidad());
            Usuario usuario = new Usuario();
            usuario.setClave(encodedString);
            usuario.setUsuario(persona.getNumeroDocIdentidad());
            usuario.setEstado(EstadoEnum.ACT.name());
            usuario.setFechaRegistro(new Date());
            usuario.setIdUserRegistro(1l);
            usuario.setPersona(persona);
            usuarioDAO.save(usuario);

            for (String cursoString : persona.getListCursos()) {
                Curso curso = cursoDAO.findCodigo(cursoString);

                PersonaCurso personaCurso = new PersonaCurso();
                personaCurso.setCurso(curso);
                personaCurso.setPersona(persona);
                personaCursoDAO.save(personaCurso);
            }

            persona.setUserGenerado(Boolean.TRUE);
            personaDAO.update(persona);

            UsuarioRol usuarioRol = new UsuarioRol();
            usuarioRol.setRol(rol);
            usuarioRol.setUsuario(usuario);
            usuarioRol.setFechaInicio(persona.getFechaIngreso());
            usuarioRol.setEstado(ACT.name());
            usuarioRol.setFechaRegistro(new Date());
            usuarioRolDAO.save(usuarioRol);
        }
    }

    @Override
    public void convertCsv(MultipartFile multipart) throws Exception {
        InputStream inp = null;

        inp = new FileInputStream(saveFileAbonos(multipart));
        String fileName = TypesUtil.getUnixTime() + "." + "personal.csv";
        FileHelper.createDirectory(FILE_DIR);
        String absoluteName = FILE_DIR + fileName;
        File outputFile = new File(absoluteName);

        FileOutputStream fos = new FileOutputStream(outputFile);

        Workbook wb = WorkbookFactory.create(inp);
        Sheet my_worksheet = wb.getSheetAt(0);
        Iterator<Row> rowIterator = my_worksheet.iterator();

        StringBuffer data = new StringBuffer();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            System.err.println("ROW " + row.getRowNum());
            if (row.getRowNum() < 3) {
                continue;
            }
            int i = 0;//String array
            //change this depending on the length of your sheet
//            String[] csvdata = new String[2];
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next(); //Fetch CELL
                System.err.println("cell " + cell.getCellType());

                switch (cell.getCellType()) { //Identify CELL type
                    //you need to add more code here based on
                    //your requirement / transformations
                    case Cell.CELL_TYPE_STRING:
//                        csvdata[i] = cell.getStringCellValue();
                        data.append(cell.getStringCellValue().trim() + ";");
                        System.err.println("ROW " + row.getRowNum() + " cell " + cell.getStringCellValue());
                        break;

                    case Cell.CELL_TYPE_BOOLEAN:
                        data.append(cell.getBooleanCellValue() + ";");
                        System.err.println("ROW " + row.getRowNum() + " cell " + cell.getBooleanCellValue());

                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            System.out.print(dateFormat.format(cell.getDateCellValue()) + ";");
                            data.append(dateFormat.format(cell.getDateCellValue()) + ";");
                        } else {
                            System.out.print(cell.getNumericCellValue() + ";");
                            Double sa = cell.getNumericCellValue();
                            data.append(sa.intValue() + ";");
                        }
                        System.err.println("ROW " + row.getRowNum() + " cell " + cell.getNumericCellValue());
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        data.append("" + ";");
                        System.err.println("ROW " + row.getRowNum() + " cell " + "");
                        break;
                    default:
                        data.append(cell + ";");
                }
                i = i + 1;
            }
            data.append("\r\n");
        }
        //we created our file..!!
        inp.close(); //close xls
        fos.write(data.toString().getBytes());
        fos.close();
        this.procesar(absoluteName);

    }

    private void procesar(String file) {
        readExcelDAO.procesarCsv(file);
    }

    private String saveFileAbonos(MultipartFile file) {
        try {
            String fileName = TypesUtil.getUnixTime() + "." + TypesUtil.getClean(file.getOriginalFilename());
            FileHelper.createDirectory(TMP_DIR);
            String absoluteName = TMP_DIR + fileName;

            FileHelper.saveToDisk(file, absoluteName);
            return absoluteName;
        } catch (IOException ex) {
            throw new PhobosException("Archivo no puede ser ubicado en el servidor");
        }
    }

    @Override
    @Transactional
    public void deleteAll() {
        readExcelDAO.deleteAll();
    }

    @Override
    @Transactional
    public Persona loadFile(MultipartFile file, Persona persona) {
        persona = personaDAO.find(persona);
        try {
            String fileExt = TypesUtil.getClean(FilenameUtils.getExtension(file.getOriginalFilename())).toLowerCase();
            String fileName = TypesUtil.getUnixTime() + "." + fileExt;
            String absoluteName = TMP_DIR + fileName;
            FileHelper.saveToDisk(file, absoluteName);
            String ruta = S3_LINK + fileName;
            persona.setRutaFoto(ruta);
            personaDAO.update(persona);

        } catch (IOException ex) {
            Logger.getLogger(GenerarInfoServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return persona;
    }

}
