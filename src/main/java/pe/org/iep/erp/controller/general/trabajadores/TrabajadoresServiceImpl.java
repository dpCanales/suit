package pe.org.iep.erp.controller.general.trabajadores;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.org.iep.erp.dao.general.PersonaCursoDAO;
import pe.org.iep.erp.dao.general.PersonaDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioDAO;
import pe.org.iep.erp.model.enums.EstadoEnum;
import static pe.org.iep.erp.model.enums.EstadoEnum.INA;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.PersonaCurso;
import pe.org.iep.erp.model.seguridad.Usuario;

@Service
@Transactional
public class TrabajadoresServiceImpl implements TrabajadoresService {

    @Autowired
    PersonaDAO personaDAO;
    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    PersonaCursoDAO personaCursoDAO;

    @Override
    public List<Persona> allPersona(DynatableFilter filter) {
        List<Persona> personas = personaDAO.allByDynatable(filter);
        List<PersonaCurso> personaCursos = personaCursoDAO.allByPersonas(personas);
        for (Persona persona : personas) {
            persona.setCursos(personaCursos.stream().filter(x -> x.getPersona().getId() == persona.getId()).map(x -> x.getCurso()).collect(Collectors.toList()));
        }
        return personas;
    }

    @Override
    @Transactional
    public void retirar(Persona persona, Usuario user) {
        persona = personaDAO.find(persona);
        persona.setFechaRetiro(new Date());
        persona.setEstadoEnum(EstadoEnum.INA);
        personaDAO.update(persona);
        
        Usuario usuario = usuarioDAO.findByPersona(persona);
        usuario.setEstado(INA.name());
        usuarioDAO.update(usuario);
        
    }

}
