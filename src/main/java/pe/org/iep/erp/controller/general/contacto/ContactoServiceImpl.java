package pe.org.iep.erp.controller.general.contacto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.albatross.zelpers.miscelanea.ListsInspector;
import pe.albatross.zelpers.miscelanea.ObjectUtil;
import pe.albatross.zelpers.miscelanea.TypesUtil;
import pe.org.iep.erp.dao.general.CargoDAO;
import pe.org.iep.erp.dao.general.CursoDAO;
import pe.org.iep.erp.dao.general.PaisDAO;
import pe.org.iep.erp.dao.general.PersonaCursoDAO;
import pe.org.iep.erp.dao.general.PersonaDAO;
import pe.org.iep.erp.dao.general.ProvinciaDAO;
import pe.org.iep.erp.dao.general.TipoDocumentoDAO;
import pe.org.iep.erp.dao.seguridad.RolDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioDAO;
import pe.org.iep.erp.dao.seguridad.UsuarioRolDAO;
import pe.org.iep.erp.model.enums.EstadoEnum;
import static pe.org.iep.erp.model.enums.EstadoEnum.ACT;
import pe.org.iep.erp.model.general.Suit.Cargo;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Pais;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.PersonaCurso;
import pe.org.iep.erp.model.general.Suit.Provincia;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;
import pe.org.iep.erp.model.seguridad.Rol;
import pe.org.iep.erp.model.seguridad.Usuario;
import pe.org.iep.erp.model.seguridad.UsuarioRol;

@Service
@Transactional
public class ContactoServiceImpl implements ContactoService {

    @Autowired
    PersonaDAO personaDAO;
    @Autowired
    CargoDAO cargoDAO;
    @Autowired
    PaisDAO paisDAO;
    @Autowired
    ProvinciaDAO provinciaDAO;
    @Autowired
    CursoDAO cursoDAO;
    @Autowired
    PersonaCursoDAO personaCursoDAO;
    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    TipoDocumentoDAO tipoDocumentoDAO;
    @Autowired
    RolDAO rolDAO;
    @Autowired
    UsuarioRolDAO usuarioRolDAO;

    @Override
    public Persona findPersona(Persona persona) {
        persona = personaDAO.find(persona);
        List<PersonaCurso> personaCursos = personaCursoDAO.findByPersona(persona);
        persona.setCursos(personaCursos.stream().map(x -> x.getCurso()).collect(Collectors.toList()));
        Usuario usuario = usuarioDAO.findByPersona(persona);
        List<Rol> roles = usuarioRolDAO.allByUsuario(usuario).stream().map(x -> x.getRol()).collect(Collectors.toList());
        persona.setRoles(roles);
        return persona;
    }

    @Override
    @Transactional
    public void update(Persona personaForm) {
        ObjectUtil.eliminarAttrSinId(personaForm);
        personaForm.setNombreCompleto(personaForm.getNombreCompleto());
        personaDAO.update(personaForm);

    }

    @Override
    @Transactional
    public void updatePersona(Persona personaForm) {
        ObjectUtil.eliminarAttrSinId(personaForm);
        personaForm.setNombreCompleto(personaForm.getNombreCompleto());

        List<PersonaCurso> persCursoNew = new ArrayList();
        for (Curso curso : personaForm.getCursos()) {
            PersonaCurso personaCurso = new PersonaCurso();
            personaCurso.setPersona(personaForm);
            personaCurso.setCurso(curso);
//            personaCurso.setFechaRegistro(new Date());
            persCursoNew.add(personaCurso);
        }
        List<PersonaCurso> personaCursoDB = personaCursoDAO.findByPersona(personaForm);

        ListsInspector listsInspector = TypesUtil.analizeLists(personaCursoDB, persCursoNew, "curso.id");
        List<PersonaCurso> personaCursoDead = listsInspector.getDeadList();
        List<PersonaCurso> personaCursoNew = listsInspector.getNewList();
        List<PersonaCurso> personaCursoOld = listsInspector.getOldListDB();
        for (PersonaCurso personaCur : personaCursoDead) {
            personaCursoDAO.delete(personaCur);
        }
        String cursosConcat = "";
        for (PersonaCurso personaCurso : personaCursoNew) {
            personaCurso.setFechaRegistro(new Date());
            cursosConcat = cursosConcat.concat(personaCurso.getCurso().getCodigo()).concat("/");
            personaCursoDAO.save(personaCurso);
        }
        for (PersonaCurso personaCurso : personaCursoOld) {
            cursosConcat = cursosConcat.concat(personaCurso.getCurso().getCodigo()).concat("/");
        }
        personaForm.setCursosConcat(cursosConcat.substring(0, cursosConcat.length() - 1));
        personaDAO.update(personaForm);

        Usuario usuario = usuarioDAO.findByPersona(personaForm);
        List<UsuarioRol> usuarioRolsNew = new ArrayList();
        for (Rol role : personaForm.getRoles()) {
            UsuarioRol usuarioRol = new UsuarioRol();
            usuarioRol.setRol(role);
            usuarioRol.setUsuario(usuario);
            usuarioRol.setFechaInicio(personaForm.getFechaIngreso());
            usuarioRol.setEstado(ACT.name());
            usuarioRol.setFechaRegistro(new Date());
            usuarioRolsNew.add(usuarioRol);
        }

        List<UsuarioRol> usuarioRolsDB = usuarioRolDAO.allByUsuario(usuario);

        ListsInspector listsInspectorUser = TypesUtil.analizeLists(usuarioRolsDB, usuarioRolsNew, "rol.id");
        List<UsuarioRol> usuarioRolDead = listsInspectorUser.getDeadList();
        List<UsuarioRol> usuarioRolNew = listsInspectorUser.getNewList();
        for (UsuarioRol usuarioRol : usuarioRolDead) {
            usuarioRolDAO.delete(usuarioRol);
        }
        for (UsuarioRol usuarioRol : usuarioRolNew) {
            usuarioRolDAO.save(usuarioRol);
        }

    }

    @Override
    public List<Cargo> allCargo() {

        return cargoDAO.all();
    }

    @Override
    public List<Pais> allPaises(String nombre) {

        return paisDAO.allByName(nombre);
    }

    @Override
    public List<Provincia> allProvincias(String nombre) {

        return provinciaDAO.allByNombre(nombre);
    }

    @Override
    public List<Curso> allCursos() {
        return cursoDAO.all();
    }

    @Override
    @Transactional
    public void save(Persona persona) {
        ObjectUtil.eliminarAttrSinId(persona);
        persona.setEstadoEnum(EstadoEnum.ACT);
        persona.setFechaIngreso(new Date());
        persona.setNombreCompleto(persona.getNombreCompleto());
        personaDAO.save(persona);

        for (Curso curso : persona.getCursos()) {
            PersonaCurso personaCurso = new PersonaCurso();
            personaCurso.setPersona(persona);
            personaCurso.setFechaRegistro(new Date());
            personaCurso.setCurso(curso);
            personaCursoDAO.save(personaCurso);
        }

        String encodedString = DigestUtils.md5Hex(persona.getNumeroDocIdentidad());
        Usuario usuario = new Usuario();
        usuario.setClave(encodedString);
        usuario.setUsuario(persona.getNumeroDocIdentidad());
        usuario.setEstado(EstadoEnum.ACT.name());
        usuario.setFechaRegistro(new Date());
        usuario.setIdUserRegistro(1l);
        usuario.setPersona(persona);
        usuarioDAO.save(usuario);

        for (Rol role : persona.getRoles()) {
            UsuarioRol usuarioRol = new UsuarioRol();
            usuarioRol.setRol(role);
            usuarioRol.setUsuario(usuario);
            usuarioRol.setFechaInicio(persona.getFechaIngreso());
            usuarioRol.setEstado(ACT.name());
            usuarioRol.setFechaRegistro(new Date());
            usuarioRolDAO.save(usuarioRol);
        }

    }

    @Override
    public List<TipoDocIdentidad> allTipoDocumento() {
        return tipoDocumentoDAO.all();
    }

    @Override
    public Persona searhPerson(Persona personaForm) {
        Persona persona = personaDAO.findByTipoDoc(personaForm);
        if (persona != null) {
            List<PersonaCurso> personaCursos = personaCursoDAO.findByPersona(persona);
            persona.setCursos(personaCursos.stream().map(x -> x.getCurso()).collect(Collectors.toList()));
        }
        return persona;
    }

    @Override
    public List<Rol> allRol() {
        return rolDAO.all();
    }

}
