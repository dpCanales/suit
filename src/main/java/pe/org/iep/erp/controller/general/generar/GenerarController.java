package pe.org.iep.erp.controller.general.generar;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import pe.albatross.zelpers.miscelanea.JsonHelper;
import pe.albatross.zelpers.miscelanea.JsonResponse;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.zelper.general.Constantine;
import static pe.org.iep.erp.zelper.general.Constantine.SESSION_USUARIO;
import pe.org.iep.erp.zelper.general.DataSession;

@Controller
@RequestMapping("general/info")
public class GenerarController {

    @Autowired
    GenerarInfoService generarInfoService;

    @RequestMapping
    public String index() {
        return "test/index";
    }

    @ResponseBody
    @RequestMapping("generar")
    public JsonResponse generarAbono(HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);

            generarInfoService.generar();
            generarInfoService.generarUsuario();
            response.setSuccess(Boolean.TRUE);
            response.setMessage("Se generaron personales satisfactoriamente");

        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("subirArchivo")
    public JsonResponse subirArchivo(@RequestParam("file") MultipartFile file, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            generarInfoService.deleteAll();
            generarInfoService.convertCsv(file);
            response.setSuccess(Boolean.TRUE);
            response.setMessage("Se procesó correctamente el archivo");

        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage(e.getMessage());
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("loadFile")
    public JsonResponse loadFile(@RequestParam(value = "file") MultipartFile file, @RequestParam("id") Long id, HttpSession session) {
        JsonResponse json = new JsonResponse();
        DataSession ds = (DataSession) session.getAttribute(Constantine.SESSION_USUARIO);
        try {
            Persona persona = generarInfoService.loadFile(file, new Persona(id));
            json.setData(personaJson(persona));
            json.setSuccess(Boolean.TRUE);
            json.setMessage("Se cargó el archivo satisfactoriamente.");

        } catch (Exception e) {
            e.printStackTrace();
            json.setTotal(0);
        }

        return json;
    }

    private ObjectNode personaJson(Persona persona) {
        ObjectNode node = JsonHelper.createJson(persona, JsonNodeFactory.instance, new String[]{
            "*",
            "cargo.*",
            "provinciaNacer.*",
            "paisNacer.*",
            "tipoDocumentoIdentidad.*"
        });

        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        List<Curso> list = new ArrayList<>();
        persona.setCursos(list);
        for (Curso cur : persona.getCursos()) {
            arrayNode.add(JsonHelper.createJson(cur, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        node.set("personasCargo", arrayNode);

        return node;
    }
}
