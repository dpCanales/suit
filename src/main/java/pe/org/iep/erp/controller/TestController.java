package pe.org.iep.erp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dashboard")
public class TestController {

    @RequestMapping
    public String index() {
        return "test/index";
    }

}
