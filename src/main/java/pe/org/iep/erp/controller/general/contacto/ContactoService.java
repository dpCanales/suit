package pe.org.iep.erp.controller.general.contacto;

import java.util.List;
import pe.org.iep.erp.model.general.Suit.Cargo;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Pais;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.Provincia;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;
import pe.org.iep.erp.model.seguridad.Rol;

public interface ContactoService {

    public Persona findPersona(Persona persona);

    public void update(Persona persona);

    public List<Cargo> allCargo();

    public List<Pais> allPaises(String nombre);

    public List<Provincia> allProvincias(String nombre);

    public List<Curso> allCursos();

    public void save(Persona persona);

    public List<TipoDocIdentidad> allTipoDocumento();

    public Persona searhPerson(Persona personaForm);

    public void updatePersona(Persona persona);

    public List<Rol> allRol();

}
