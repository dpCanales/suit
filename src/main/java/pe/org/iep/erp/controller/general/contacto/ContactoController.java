package pe.org.iep.erp.controller.general.contacto;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pe.albatross.zelpers.miscelanea.JsonHelper;
import pe.albatross.zelpers.miscelanea.JsonResponse;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.org.iep.erp.model.general.Suit.Cargo;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Pais;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.general.Suit.Provincia;
import pe.org.iep.erp.model.general.Suit.TipoDocIdentidad;
import pe.org.iep.erp.model.seguridad.Rol;
import static pe.org.iep.erp.zelper.general.Constantine.SESSION_USUARIO;
import pe.org.iep.erp.zelper.general.DataSession;
import pe.org.iep.erp.zelper.pdf.PDFFormatoEnum;
import pe.org.iep.erp.zelper.pdf.PdfHtmlView;

@Controller
@RequestMapping("general/contacto")
public class ContactoController {

    @Autowired
    ContactoService service;

    @Autowired
    PdfHtmlView pdfHtmlView;

    @RequestMapping
    public String index(Model model, HttpSession session) {
        DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
        Persona persona = service.findPersona(ds.getPersona());
        List<Cargo> cargos = service.allCargo();
        List<Curso> cursos = service.allCursos();
        List<TipoDocIdentidad> tipoDocIdentidads = service.allTipoDocumento();
        model.addAttribute("persona", personaJson(persona));
        model.addAttribute("cursos", cursosJson(cursos));
        model.addAttribute("cargos", cargosJson(cargos));
        model.addAttribute("tipoDocumentos", tipoDocumentosJson(tipoDocIdentidads));
        return "general/contacto/contactoForm";
    }

    @RequestMapping("nuevo")
    public String nuevo(Model model, HttpSession session) {
        DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
        Persona persona = new Persona();
        List<Cargo> cargos = service.allCargo();
        List<Curso> cursos = service.allCursos();
        List<Rol> roles = service.allRol();
        List<TipoDocIdentidad> tipoDocIdentidads = service.allTipoDocumento();

        model.addAttribute("roles", rolJson(roles));
        model.addAttribute("persona", personaJson(persona));
        model.addAttribute("cursos", cursosJson(cursos));
        model.addAttribute("cargos", cargosJson(cargos));
        model.addAttribute("tipoDocumentos", tipoDocumentosJson(tipoDocIdentidads));
        return "general/contacto/contactoNuevoForm";
    }

    @RequestMapping("editar/{id}")
    public String editar(@PathVariable Long id, Model model, HttpSession session) {
        DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
        Persona persona = service.findPersona(new Persona(id));
        List<Cargo> cargos = service.allCargo();
        List<Curso> cursos = service.allCursos();
        List<TipoDocIdentidad> tipoDocIdentidads = service.allTipoDocumento();
        List<Rol> roles = service.allRol();

        model.addAttribute("roles", rolJson(roles));
        model.addAttribute("persona", personaJson(persona));
        model.addAttribute("cursos", cursosJson(cursos));
        model.addAttribute("cargos", cargosJson(cargos));
        model.addAttribute("tipoDocumentos", tipoDocumentosJson(tipoDocIdentidads));
        return "general/contacto/contactoNuevoForm";
    }

    @ResponseBody
    @RequestMapping("update")
    public JsonResponse update(@RequestBody Persona persona, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            service.update(persona);

            response.setSuccess(Boolean.TRUE);
            response.setMessage("Se actualizaron sus datos satisfactoriamente.");
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("updatePersona")
    public JsonResponse updatePersona(@RequestBody Persona persona, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            service.updatePersona(persona);

            response.setSuccess(Boolean.TRUE);
            response.setMessage("Se actualizaron sus datos satisfactoriamente.");
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("save")
    public JsonResponse save(@RequestBody Persona persona, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            service.save(persona);

            response.setSuccess(Boolean.TRUE);
            response.setMessage("Se registraron sus datos satisfactoriamente.");
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("searhPerson")
    public JsonResponse searhPerson(@RequestBody Persona personaForm, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            Persona persona = service.searhPerson(personaForm);
            if (persona == null) {
                persona = personaForm;
            }
            response.setData(personaJson(persona));
            response.setSuccess(Boolean.TRUE);
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("allPaises")
    public JsonResponse allPaises(@RequestParam(value = "nombre") String nombre, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            List<Pais> paises = service.allPaises(nombre);

            response.setData(paisesJson(paises));
            response.setSuccess(Boolean.TRUE);
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @ResponseBody
    @RequestMapping("allProvincias")
    public JsonResponse allProvincias(@RequestParam(value = "nombre") String nombre, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            List<Provincia> provincias = service.allProvincias(nombre);

            response.setData(provinciaJson(provincias));
            response.setSuccess(Boolean.TRUE);
        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    @RequestMapping("downloadPDF/{id}")
    public ModelAndView downloadPDF(@PathVariable Long id, HttpSession session, HttpServletResponse respons, RedirectAttributes redirectAttr, Model model) {
        Persona persona = service.findPersona(new Persona(id));
//
//        String estimado = persona.esFemenino() ? "Estimada" : "Estimado";
//
//        String carta = cabeceraBoletaPdf.getContenido();
//
//        carta = carta.replace(NOMBRE_PERSONA.getValue(), persona.getNombreCompleto());
//        carta = carta.replace(ESTIMADO.getValue(), estimado);
//        carta = carta.replace(NRO_DOCUMENTO.getValue(), persona.getNumeroDocIdentidad());
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        model.addAttribute("nombre", persona.getNombreCompleto());
        model.addAttribute("rutaFoto", persona.getRutaFoto());
        model.addAttribute("formatoEnum", PDFFormatoEnum.FOTO_CHECK);
        model.addAttribute("nombrePdf", "Fotocheck - " + persona.getNombreCompleto());
        model.addAttribute("tipoDocumento", persona.getTipoDocumentoIdentidad().getSimbolo());
        model.addAttribute("numeroDocumento", persona.getNumeroDocIdentidad());
        model.addAttribute("sexo", persona.getSexo());
        model.addAttribute("edad", persona.getEdad());
        model.addAttribute("celular", persona.getCelular());
        if (persona.getFechaNacer() != null) {
            model.addAttribute("fechaNacer", dateFormat.format(persona.getFechaNacer()));
        }
        if (persona.getFechaIngreso() != null) {
            model.addAttribute("fechaIngreso", dateFormat.format(persona.getFechaIngreso()));
        }
        model.addAttribute("ultimoTrabajo", persona.getUltimoWork());
        model.addAttribute("hobbie", persona.getHobby());
        model.addAttribute("cursos", persona.getCursosConcat());
        model.addAttribute("cargo", persona.getCargo().getNombre());
        return new ModelAndView(pdfHtmlView);
//        return null;
    }

    private ArrayNode cargosJson(List<Cargo> cargos) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Cargo cargo : cargos) {
            node.add(JsonHelper.createJson(cargo, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ArrayNode paisesJson(List<Pais> paises) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Pais pais : paises) {
            node.add(JsonHelper.createJson(pais, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ArrayNode provinciaJson(List<Provincia> provincias) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Provincia provincia : provincias) {
            node.add(JsonHelper.createJson(provincia, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ArrayNode cursosJson(List<Curso> cursos) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Curso curso : cursos) {
            node.add(JsonHelper.createJson(curso, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ArrayNode rolJson(List<Rol> roles) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Rol rol : roles) {
            node.add(JsonHelper.createJson(rol, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ArrayNode tipoDocumentosJson(List<TipoDocIdentidad> tipoDocumentos) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (TipoDocIdentidad tipoDoc : tipoDocumentos) {
            node.add(JsonHelper.createJson(tipoDoc, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }

    private ObjectNode personaJson(Persona persona) {
        ObjectNode node = JsonHelper.createJson(persona, JsonNodeFactory.instance, new String[]{
            "*",
            "cargo.*",
            "provinciaNacer.*",
            "paisNacer.*",
            "tipoDocumentoIdentidad.*"
        });

        ArrayNode arrayNode = new ArrayNode(JsonNodeFactory.instance);
        ArrayNode arrayNodeRol = new ArrayNode(JsonNodeFactory.instance);
        List<Curso> list = new ArrayList<>();
        List<Rol> listRol = new ArrayList<>();
        if (persona.getCursos() == null) {
            persona.setCursos(list);
        }
        if (persona.getRoles() == null) {
            persona.setRoles(listRol);
        }
        for (Curso cur : persona.getCursos()) {
            arrayNode.add(JsonHelper.createJson(cur, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        for (Rol role : persona.getRoles()) {
            arrayNodeRol.add(JsonHelper.createJson(role, JsonNodeFactory.instance, new String[]{
                "*"
            }));

        }
        node.set("cursos", arrayNode);
        node.set("roles", arrayNodeRol);

        return node;
    }
}
