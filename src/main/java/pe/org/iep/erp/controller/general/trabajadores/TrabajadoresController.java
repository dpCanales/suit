package pe.org.iep.erp.controller.general.trabajadores;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.albatross.octavia.dynatable.DynatableResponse;
import pe.albatross.zelpers.miscelanea.JsonHelper;
import pe.albatross.zelpers.miscelanea.JsonResponse;
import pe.albatross.zelpers.miscelanea.PhobosException;
import pe.org.iep.erp.controller.general.contacto.ContactoService;
import static pe.org.iep.erp.model.enums.RolEnum.ADM;
import pe.org.iep.erp.model.general.Suit.Cargo;
import pe.org.iep.erp.model.general.Suit.Curso;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Rol;
import static pe.org.iep.erp.zelper.general.Constantine.SESSION_USUARIO;
import pe.org.iep.erp.zelper.general.DataSession;

@Controller
@RequestMapping("general/trabajadores")
public class TrabajadoresController {

    @Autowired
    TrabajadoresService service;
    @Autowired
    ContactoService contactoService;

    @RequestMapping
    public String index(Model model, HttpSession session) {
        DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
        List<Cargo> cargos = contactoService.allCargo();
        Boolean isAdmin = false;
        for (Rol role : ds.getRoles()) {
            if (role.getCodigo().equals(ADM.name())) {
                isAdmin = true;
            }
        }
        model.addAttribute("cargos", cargosJson(cargos));
        model.addAttribute("isAdmin", isAdmin);
        return "general/trabajadores/trabajadores";
    }

    @ResponseBody
    @RequestMapping("list")
    public DynatableResponse list(DynatableFilter filter, HttpSession session) {

        DynatableResponse response = new DynatableResponse();

        DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
        List<Persona> personas = service.allPersona(filter);

        response.setData(personasJson(personas));
        response.setFiltered(filter.getFiltered());
        response.setTotal(filter.getTotal());

        return response;
    }

    @ResponseBody
    @RequestMapping("retirar")
    public JsonResponse retirar(@RequestBody Persona persona, HttpSession session) {

        JsonResponse response = new JsonResponse();
        try {
            DataSession ds = (DataSession) session.getAttribute(SESSION_USUARIO);
            service.retirar(persona, ds.getUsuario());

            response.setMessage("Se actualizó el registro satisfactoriamente.");
            response.setSuccess(Boolean.TRUE);

        } catch (PhobosException e) {
            response.setMessage(e.getLocalizedMessage());
        } catch (Exception e) {
            response.setMessage("Se presentó un problema. Por favor comuníquese con mesa de ayuda.");
        }
        return response;
    }

    private ArrayNode personasJson(List<Persona> personas) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Persona persona : personas) {
            ArrayNode nodeCursos = new ArrayNode(JsonNodeFactory.instance);
            ObjectNode objectNode = JsonHelper.createJson(persona, JsonNodeFactory.instance, new String[]{
                "*",
                "cargo.*",
                "tipoDocumentoIdentidad.*"
            });
            for (Curso cur : persona.getCursos()) {
                nodeCursos.add(JsonHelper.createJson(cur, JsonNodeFactory.instance, new String[]{
                    "*"
                }));
            }
            objectNode.set("cursos", nodeCursos);
            node.add(objectNode);
        }

        return node;
    }

    private ArrayNode cargosJson(List<Cargo> cargos) {

        ArrayNode node = new ArrayNode(JsonNodeFactory.instance);
        for (Cargo cargo : cargos) {
            node.add(JsonHelper.createJson(cargo, JsonNodeFactory.instance, new String[]{
                "*"
            }));
        }
        return node;
    }
}
