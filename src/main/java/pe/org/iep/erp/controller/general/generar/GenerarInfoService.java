package pe.org.iep.erp.controller.general.generar;

import org.springframework.web.multipart.MultipartFile;
import pe.org.iep.erp.model.general.Suit.Persona;

public interface GenerarInfoService {

    public void generar();

    public void generarUsuario();

    public void convertCsv(MultipartFile file) throws Exception;

    public void deleteAll();

    public Persona loadFile(MultipartFile file, Persona persona);

}
