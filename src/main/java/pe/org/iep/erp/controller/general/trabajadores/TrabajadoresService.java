package pe.org.iep.erp.controller.general.trabajadores;

import java.util.List;
import pe.albatross.octavia.dynatable.DynatableFilter;
import pe.org.iep.erp.model.general.Suit.Persona;
import pe.org.iep.erp.model.seguridad.Usuario;

public interface TrabajadoresService {

    public List<Persona> allPersona(DynatableFilter filter);

    public void retirar(Persona persona, Usuario usuario);

}
