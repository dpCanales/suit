$(function () {
    $('input').attr('autocomplete', 'off');

    $('.scrollable').scroll(function () {

        var limit = ($('#profileContainer').length === 1) ? 230 : 0;
        var nav = $('.subbar');

        if ($(this).scrollTop() > limit) {

            nav.addClass("f-nav");
        } else {

            nav.removeClass("f-nav");
        }
    });
});

Messenger.options = {
    extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
    theme: 'flat',
}

function notify(message, type) {
    var t = (type == null) ? 'success' : type;
    setTimeout(function () {
        Messenger().post({
            message: message,
            type: t,
            hideAfter: 12,
            showCloseButton: true
        });
    }, 900);
}

const message = {
    info(message) {
        notify(message, 'info');
    },
    error(message) {
        notify(message, 'error');
    }
}

function randString(n) {
    if (!n) {
        n = 5;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < n; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

// $.fn.datepicker.defaults.format = "dd/mm/yyyy";
// $.fn.datepicker.defaults.language = "es";
// $.fn.datepicker.defaults.autoclose = true;
// $.fn.datepicker.defaults.todayHighlight = false;
// $.fn.datepicker.defaults.showButtonPanel = false;


APP = {
    cleanForm: function (f) {
        f.find("input[type=text], textarea, #id").val("");
        f.find("input[type=checkbox]").prop("checked", false);
        f.find("input[name='*[]']").prop("checked", false);
        f.find("input[name='id*']").val("");
    },
    cleanAll: function (f) {
        f.find("input, textarea").val("");
    },
    fillFormById: function (data, f) {
        $.each(data, function (index, value) {
            f.find('#' + index).val(value);
        });
    },
    fillFormByName: function (data, f) {
        $.each(data, function (index, value) {
            f.find('[name="' + index + '"]').val(value);
        });
    },
//    select2: function () {
//        $(".select2single").select2({minimumResultsForSearch: -1});
//        $(".select2").select2()
//    },
    url: function (relative) {
        return contextPath + relative;
    },
    datePicker: {format: 'dd/mm/yyyy', language: 'es', autoclose: true, todayHighlight: true},
    timePicker: {
        minuteStep: 5,
        showInputs: true,
        disableFocus: true,
        showSeconds: false,
        showMeridian: false,
    }

}

MESSAGES = {
    errorComunicacion: 'Error de conexión con el servidor.',
    confirmDelete: '¿Seguro que desea eliminar el registro?'
}

function toMoney(amount) {
    let temp = new Number(amount).toFixed(2);
    if (isNaN(temp))
        temp = new Number(0).toFixed(2);
    return temp.replace(/\d(?=(\d{3})+\.)/g, '$&,');
}

function reduce(numerator, denominator) {
    var gcd = function gcd(a, b) {
        return b ? gcd(b, a % b) : a;
    };
    gcd = gcd(numerator, denominator);
    return [numerator / gcd, denominator / gcd];
}

function range(min, max) {
    var array = [],
            j = 0;
    for (var i = min; i <= max; i++) {
        array[j] = i;
        j++;
    }
    return array;
}
//APP.select2();

Vue.filter('monetize', function (value) {
    if (value === undefined || value === null)
        return ''
    let val = new Number(value);
    if (isNaN(val)) {
        return '0.00';
    } else {
        if (val === 0) {
            return '0.00';
        } else {
            return val.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        }
    }
})

const AXIOS = axios.create({
//    timeout: 1000,
});

AXIOS.interceptors.response.use(function (response) {
    if (!response.data.success) {
        notify(response.data.message, 'error');
    } else {
        if (response.data.message) {
            notify(response.data.message, 'info');
        }
    }
    return response;
}, function (error) {
    notify(MESSAGES.errorComunicacion, 'error');
    return Promise.reject(error);
});

Vue.filter('currency', function (value) {
    return parseFloat(value).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
});



const VUEGLOBAL = {
    VueMultiselect:{
        selectedLabel: {default: ''},
        selectLabel: {default: ''},
        deselectLabel: {default: ''},
        selectGroupLabel: {default: ''},
        deselectGroupLabel: {default: ''},
        placeholder: {default: ''}
    }
    
}