
new Vue({
    el: '#changepasswordVUE',
    data: {
        saving: false,
        passwo: ''
    },
    computed: {
    },
    mounted: function () {

    },
    methods: {
        save() {
            let form = $("#form");
            if (!form.parsley().validate()) {
                return;
            }
            $.ajax({
                method: 'POST', 
                url: APP.url('security/changePass'),
                data: JSON.stringify({clave: $.md5(this.passwo)}),
                contentType: "application/json",
                success: function (response) {
                    if (response.success) {
                        message.info(response.message);
                        setTimeout(() => {
                            window.location.href = "/ventas/pedidos"
                        }, 1500)
                    } else {

                        notify(response.message, 'error');
                    }
                },
                error: function (response) {
                    notify(MESSAGES.errorComunicacion, 'error');
                }
            });
        }
    }
});







