Vue.component("multiselect", window.VueMultiselect.default);

Vue.component('usuarios-raptor', {
    data() {
        return {
            url: APP.url('seguridad/usuarios/list')
        }
    }
});


new Vue({
    el: '#usuariosVUE',
    data: {
        roles: JSON.parse(roles),
        mapRoles: new Map(),
        usuario: {},
        modal: {
            id: 'modal',
            header: 'true',
            title: 'Cambio de contraseña',
            okbtn: 'Guardar',
            cancelbtn: 'Cancelar',
            cancelclass: 'btn btn-link',
            okclass: 'btn btn-info'
        },
        modalVerRoles: {
            id: 'modalVerRoles',
            header: 'true',
            title: '',
            okbtn: 'Guardar',
            cancelbtn: 'Cerrar',
            cancelclass: 'btn btn-link',
            okclass: 'btn btn-info'
        },
        modalEditarRoles: {
            id: 'modalEditarRoles',
            header: 'true',
            title: '',
            okbtn: 'Guardar',
            cancelbtn: 'Cerrar',
            cancelclass: 'btn btn-link',
            okclass: 'btn btn-info'
        },
    },
    methods: {
        openEditarRoles(id, nombre) {
            axios.get('/seguridad/usuarios/' + id)
                .then(response => {
                    if (response.data.success) {
                        this.modalEditarRoles.title = 'Editar roles de ' + nombre;
                        this.usuario = response.data.data;
                        this.mapRoles.clear();
                        this.usuario.usuarioRol.forEach(ur => {
                            this.mapRoles.set(ur.rol.id, {...ur, encontrado: true});
                        })
                        this.roles.forEach(r => {
                            if (!this.mapRoles.has(r.id)) {
                                this.mapRoles.set(r.id, {rol: r});
                            }
                        })
                        this.$refs.modalEditarRoles.open();
                    }
                })
        },
        saveRoles() {
            let urs = Array.from(this.mapRoles.values()).filter(ur => ur.encontrado);
            let envelope = {
                ...this.usuario,
            }
            envelope.usuarioRol = urs;
            axios.post('/seguridad/usuarios/saveroles', envelope)
                .then(response => {
                    if (response.data.success) {
                        message.info(response.data.message);
                        this.$refs.table.$refs.raptor.loadRemoteData();
                        this.$refs.modalEditarRoles.close();
                    } else {
                        message.error(response.data.message);
                    }
                })

        },
        openVerRoles(id, nombre) {
            axios.get('/seguridad/usuarios/' + id)
                .then(response => {
                    if (response.data.success) {
                        this.modalVerRoles.title = 'Roles de ' + nombre;
                        this.usuario = response.data.data;
                        this.$refs.modalVerRoles.open();
                    }
                })
        },
        update() {
            axios.post('/seguridad/usuarios/updateEstado/' + this.usuario.id, this.usuario)
                .then(response => {
                    if (response.data.success) {
                        notify(response.data.message, 'info');
                        this.$refs.modal.close();
                    } else {
                        notify(response.data.message, 'error');
                    }
                }).catch(response => {
                notify(MESSAGES.errorComunicacion, 'error');
            })
        },
        activar(item) {
            this.usuario = item;
            this.$set(this.usuario, 'clave', '');
            this.usuario.estado = 'ACT';
            this.update();
        },
        desactivar(item) {
            this.usuario = item;
            this.usuario.estado = 'INA';
            this.update();
        },
        cambiarPassword(item) {
            this.usuario = item;
            this.modal.title = this.usuario.persona.nombreCompleto;
            this.$refs.modal.open();
        },
        savePassword(item) {
            let envelope = {
                id: this.usuario.id,
                clave: $.md5(this.usuario.clave)
            }
            axios.post('/seguridad/usuarios/' + envelope.id, envelope)
                .then(response => {
                    if (response.data.success) {
                        notify(response.data.message, 'info');
                    } else {
                        notify(response.data.message, 'error');
                    }
                }).catch(response => {
                notify(MESSAGES.errorComunicacion, 'error');
            })
        }

    }
});







