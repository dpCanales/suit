Vue.component("multiselect", window.VueMultiselect.default);

new Vue({
    el: '#usuariosFormVUE',
    data: {
        persona: {},
        buttonSave: true,
        usuario: {}
    },
    methods: {
        save() {
            let form = $("#form");
            if (!form.parsley().validate()) {
                return;
            }

            let $vue = this;
            let data = this.persona;
            $vue.buttonSave = false;

            let envelope = {
                persona: this.persona,
                usuario: this.usuario.usuario,
                clave: $.md5(this.usuario.clave)
            }

            axios.post('/seguridad/usuarios/', envelope)
                .then(response => {
                    if (response.data.success) {
                        notify(response.data.message, 'info');
                    } else {
                        notify(response.data.message, 'error');
                    }
                }).catch(response => {
                notify(MESSAGES.errorComunicacion, 'error');
            })

        },
        findPerson() {
            let dni = this.persona.numeroDocIdentidad;
            axios.get('/buscar/findPersonaByDNI', {
                params: {
                    dni: dni
                }
            }).then(response => {
                if (response.data.data.id)
                    this.persona = response.data.data;
                console.log(response.data.data);
            }).catch(e => {
                console.warn('Persona no encontrada');
            })
        }
    }

});
