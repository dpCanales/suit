new Vue({
    el: '#rolesFormVUE',
    data: {
        menus: JSON.parse(menus),
        menusPresentes: {},
        menuRol: [],
        menuSuperior: [],
        menusSet: {},
        rol: JSON.parse(rol),
        saving: false,
        lista1: [],
        lista2: []
    },
    computed: {

    },
    mounted: function () {
        let map = {};
        this.menuRol = [];
        let presentes = new Set();
        this.rol.menuRol.forEach(mr => {
            map[mr.menu.id] = mr.id;
            presentes.add(mr.menu.id);
        })
        this.menus.forEach(menu => {
            if (menu.menuSuperior.id) {
                if (this.menusSet[menu.menuSuperior.id] === undefined) {
                    this.menusSet[menu.menuSuperior.id] = {...menu.menuSuperior, submenus: []};
                }
                let ms = this.menusSet[menu.menuSuperior.id];
                let mr = {id: map[menu.id], menu: menu, encontrado: presentes.has(menu.id)};
                ms.submenus.push(mr);
                this.menuRol.push(mr);
            }
        })
        for (var key in this.menusSet) {
            if (this.menusSet.hasOwnProperty(key)) {
                let menu = {
                    ...this.menusSet[key],
                    id: key
                }
                if (['Ventas', 'Producción', 'Almacén'].includes(menu.nombre)) {
                    this.lista1.push(menu);
                } else {
                    this.lista2.push(menu);
                }
            }
        }
    },
    methods: {
        nuevoRol() {
            this.rol = {};
            this.convertirLista();
            this.rolesModal.title = 'Nuevo Rol';
            this.$refs.rolesModal.open();
        },
        actualizarRol(id) {
            axios.get('/seguridad/roles/find/' + id)
                .then(response => {
                    let rolBD = response.data.data;
                    this.rol = rolBD;
                    this.convertirLista(rolBD.menuRol);
                    this.rolesModal.title = 'Actualizar Rol';
                    this.$refs.rolesModal.open();

                })

        },
        convertirLista(menuRolBD = []) {
            this.menuRol = [];
            let presentes = new Set();
            menuRolBD.forEach(mr => {
                presentes.add(mr.menu.id);
            })
            this.menus.forEach(menu => {
                this.menuRol.push({menu: menu, encontrado: presentes.has(menu.id)});
            })
        },
        save() {
            let mrValido = this.menuRol.filter(mr => mr.encontrado);
            let envelope = {
                ...this.rol,
                menuRol: mrValido
            }

            this.saving = true;
            axios.post('/seguridad/roles/save', envelope)
                .then(response => {
                    if (response.data.success) {
                        message.info('Accesos actualizados');
                        setTimeout(() => {
                            window.location.href = "/seguridad/roles"
                        }, 2000);
                    } else {
                        message.error(response.data.message);
                        this.saving = false;
                    }

                })
                .catch(error => {

                })
        }
    }
});







