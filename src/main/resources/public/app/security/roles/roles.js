Vue.component('roles-raptor', {
    data() {
        return {
            url: APP.url('seguridad/roles/list')
        }
    }
});

new Vue({
    el: '#rolesVUE',
    data: {
        menusPresentes: {},
        menuRol: [],
        rol: {},
        rolesModal: {
            id: 'rolesModal',
            header: 'true',
            title: 'Actualizar Rol',
            okbtn: 'Guardar',
            cancelbtn: 'Cancelar',
            cancelclass: 'btn btn-link',
            okclass: 'btn btn-info'
        },
        accesosModal: {
            id: 'accesosModal',
            header: 'true',
            title: '',
            cancelbtn: 'Cerrar',
            cancelclass: 'btn btn-link',
        }
    },
    computed: {

    },
    mounted: function () {
    },
    methods: {
        nuevoRol() {
            this.rol = {};
//            this.convertirLista();
            this.rolesModal.title = 'Nuevo Rol';
            this.$refs.rolesModal.open();
        },
        showAccesos(id) {
            axios.get('/seguridad/roles/find/' + id)
                .then(response => {
                    let rolBD = response.data.data;
                    this.rol = rolBD;
                    this.accesosModal.title = this.rol.nombre + ': Accesos';
                    this.$refs.accesosModal.open();
                })
        },
        actualizarRol(id) {
            axios.get('/seguridad/roles/find/' + id)
                .then(response => {
                    let rolBD = response.data.data;
                    this.rol = rolBD;
                    this.rolesModal.title = 'Actualizar Rol';
                    this.$refs.rolesModal.open();

                })

        },
        convertirLista(menuRolBD = []) {
            this.menuRol = [];
            let presentes = new Set();
            menuRolBD.forEach(mr => {
                presentes.add(mr.menu.id);
            })
            this.menus.forEach(menu => {
                this.menuRol.push({menu: menu, encontrado: presentes.has(menu.id)});
            })
        },
        save() {
            axios.post('/seguridad/roles/save', this.rol)
                .then(response => {
                    message.info("Rol actualizado");
                    this.$refs.table.$refs.raptor.loadRemoteData();
                    this.$refs.rolesModal.close();
                })
        }
    }
});







