Vue.component("multiselect", window.VueMultiselect.default);
new Vue({
    el: '#contactoVue',
    data: {
        persona: JSON.parse(personaJson),
        cargos: JSON.parse(cargosJson),
        cursos: JSON.parse(cursosJson),
        roles: JSON.parse(rolesJson),
        tipoDocumentos: JSON.parse(tipoDocumentosJson),
        paises: [],
        provincias: []
    },
    computed: {
    },
    mounted: function () {
         let $vue = this;
         console.log($vue.persona);
    },
    methods: {

        save() {
            let $vue = this;
            if (!$vue.valid()) {
                return;
            }

            $.ajax({
                method: 'POST',
                url: APP.url('general/contacto/save'),
                data: JSON.stringify($vue.persona),
                contentType: "application/json",
                success: function (response) {
                    if (response.success) {
                        location.href = APP.url("general/trabajadores");
                        message.info(response.message);

                    } else {

                        notify(response.message, 'error');
                    }
                },
                error: function (response) {
                    notify(MESSAGES.errorComunicacion, 'error');
                }
            });
        },
        searchPais(search) {
            let $vue = this;
            $.ajax({
                url: APP.url('general/contacto/allPaises'),
                dataType: 'json',
                type: 'POST',
                async: true,
                data: {nombre: search},
                success(response) {
                    if (response.success) {
                        $vue.paises = response.data;
                    } else {
                        notify(response.message, "error");
                    }
                },
                error() {
                    notify(MESSAGES.errorComunicacion, "error");
                }
            });
        },
        searchProvincia(search) {
            let $vue = this;
            $.ajax({
                url: APP.url('general/contacto/allProvincias'),
                dataType: 'json',
                type: 'POST',
                async: true,
                data: {nombre: search},
                success(response) {
                    if (response.success) {
                        $vue.provincias = response.data;
                    } else {
                        notify(response.message, "error");
                    }
                },
                error() {
                    notify(MESSAGES.errorComunicacion, "error");
                }
            });
        },
        searchDoc() {
            let $vue = this;
            if ($vue.persona.tipoDocumentoIdentidad.id != null && $vue.persona.numeroDocIdentidad != null) {
                var data = {};
                data.numeroDocIdentidad = $vue.persona.numeroDocIdentidad;
                data.tipoDocumentoIdentidad = $vue.persona.tipoDocumentoIdentidad;
                data.apellidos = $vue.persona.apellidos;
                data.nombres = $vue.persona.nombres;
                $.ajax({
                    method: 'POST',
                    url: APP.url('general/contacto/searhPerson'),
                    data: JSON.stringify(data),
                    contentType: "application/json",
                    success: function (response) {
                        if (response.success) {
                            if (response.data.id != null) {
                                $vue.persona = response.data;
                            } else {
                                $vue.persona = response.data;
                                $vue.persona.numeroDocIdentidad = data.numeroDocIdentidad;
                                $vue.persona.tipoDocumentoIdentidad = data.tipoDocumentoIdentidad;
                            }
                        } else {
                            notify(response.message, 'error');
                        }
                    },
                    error: function (response) {
                        notify(MESSAGES.errorComunicacion, 'error');
                    }
                });
                console.log($vue.persona);
            }
        },
        valid() {
            let form = $("#contacto");
            if (!form.parsley().validate()) {
                return false;
            }
            return true;
        },
        update() {
            let $vue = this;
            if (!$vue.valid()) {
                return;
            }
            console.log($vue.persona);
            $.ajax({
                method: 'POST',
                url: APP.url('general/contacto/updatePersona'),
                data: JSON.stringify($vue.persona),
                contentType: "application/json",
                success: function (response) {
                    if (response.success) {

                        message.info(response.message);
                    } else {

                        notify(response.message, 'error');
                    }
                },
                error: function (response) {
                    notify(MESSAGES.errorComunicacion, 'error');
                }
            });
        }
    }
});







