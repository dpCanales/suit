Vue.component("multiselect", window.VueMultiselect.default);
new Vue({
    el: '#contactoVue',
    data: {
        persona: JSON.parse(personaJson),
        cargos: JSON.parse(cargosJson),
        cursos: JSON.parse(cursosJson),
        tipoDocumentos: JSON.parse(tipoDocumentosJson),
        paises: [],
        provincias: []
    },
    computed: {
    },
    mounted: function () {
        let $vue = this;
        console.log($vue.persona);
    },
    methods: {
        update() {
            let $vue = this;
            if (!$vue.valid()) {
                return;
            }
            console.log($vue.persona);
            $.ajax({
                method: 'POST',
                url: APP.url('general/contacto/update'),
                data: JSON.stringify($vue.persona),
                contentType: "application/json",
                success: function (response) {
                    if (response.success) {

                        message.info(response.message);
                    } else {

                        notify(response.message, 'error');
                    }
                },
                error: function (response) {
                    notify(MESSAGES.errorComunicacion, 'error');
                }
            });
        },
        searchPais(search) {
            let $vue = this;
            $.ajax({
                url: APP.url('general/contacto/allPaises'),
                dataType: 'json',
                type: 'POST',
                async: true,
                data: {nombre: search},
                success(response) {
                    if (response.success) {
                        $vue.paises = response.data;
                    } else {
                        notify(response.message, "error");
                    }
                },
                error() {
                    notify(MESSAGES.errorComunicacion, "error");
                }
            });
        },
        searchProvincia(search) {
            let $vue = this;
            $.ajax({
                url: APP.url('general/contacto/allProvincias'),
                dataType: 'json',
                type: 'POST',
                async: true,
                data: {nombre: search},
                success(response) {
                    if (response.success) {
                        $vue.provincias = response.data;
                    } else {
                        notify(response.message, "error");
                    }
                },
                error() {
                    notify(MESSAGES.errorComunicacion, "error");
                }
            });
        },
        valid() {
            let form = $("#contacto");
            if (!form.parsley().validate()) {
                return false;
            }
            return true;
        },
        handleFileUpload() {
            let $vue = this;
            this.file = $vue.$refs.file.files[0];
            var formData = new FormData();
            formData.append('file', this.file);
            formData.append('id', $vue.persona.id);
            AXIOS.post('/general/info/loadFile',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
            ).then(function (response) {
                if (response.data.success) {
                    $vue.persona = response.data.data;
                }
            }).catch(function () {
                console.log('FAILURE!!');
            });
        }
    }
});







