Vue.component("multiselect", window.VueMultiselect.default);
new Vue({
    el: '#trabajadoresVue',
    data: {
        trabajadoresURL: APP.url("general/trabajadores/list"),
        cargos: JSON.parse(cargosJson),
        isAdmin: isAdmin,
        cargo: {},
        archivoModal: {
            id: 'archivoModal',
            header: 'true',
            title: 'Subir Archivo',
            okbtn: 'Subir',
            cancelbtn: 'Cerrar',
            okclass: 'btn btn-primary',
            cancelclass: 'btn btn-link'
        }
    },
    mounted: function () {
    },
    methods: {
        retirar(item) {
            let $vue = this;
            bootbox.confirm({
                message: '<h3>¿Seguro que desea retirar al trabajador ' + item.nombreCompleto + ' ? </h3>',
                buttons: {
                    confirm: {label: 'Si, retirar', className: "btn-danger"},
                    cancel: {label: 'Cancelar', className: "btn-link"}
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            method: 'POST',
                            url: APP.url('general/trabajadores/retirar'),
                            contentType: "application/json",
                            data: JSON.stringify(item),
                            success: function (response) {
                                if (response.success) {
                                    $vue.$refs.trabajadorRaptor.loadRemoteData();
                                    notify(response.message, "success");
                                }
                            },
                            error: function () {
                                notify(MESSAGES.errorComunicacion, "error");
                                MODAL.hideWait();
                            }
                        });
                    }
                }
            });

        },
        openModal() {
            let $vue = this;
            $vue.$refs.archivoModal.open();
        },
        subirArchivo() {
            let $vue = this;
            var formData = new FormData();
            formData.append('file', $vue.file);
            AXIOS.post('/general/info/subirArchivo',
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }
            ).then(function (response) {
                if (response.data.success) {
                    $vue.$refs.archivoModal.close();
                }
            }).catch(function () {
                console.log('FAILURE!!');
            });
        }, getFile() {

            this.file = this.$refs.file.files[0];
        },
        procesarArchivo() {
            let $vue = this;
            bootbox.confirm({
                message: '<h3>¿Seguro que desea procesar el archivo ? </h3>',
                buttons: {
                    confirm: {label: 'Si, procesar', className: "btn-danger"},
                    cancel: {label: 'Cancelar', className: "btn-link"}
                },
                callback: function (result) {
                    if (result) {
                        $.ajax({
                            method: 'POST',
                            url: APP.url('general/info/generar'),
                            contentType: "application/json",
                            success: function (response) {
                                if (response.success) {
                                    $vue.$refs.trabajadorRaptor.loadRemoteData();
                                    notify(response.message, "success");
                                }
                            },
                            error: function () {
                                notify(MESSAGES.errorComunicacion, "error");
                            }
                        });
                    }
                }
            });
        },
        updateUrl(item) {
            location.href = APP.url("general/contacto/editar/" + item.id);
        },
        changeRaptor(item) {
            let $vue = this;

            if (item != null) {
                $vue.$refs.trabajadorRaptor.querie.push({name: 'cargo', value: item.id});
            } else {
                $vue.$refs.trabajadorRaptor.querie = [];
            }
            $vue.$refs.trabajadorRaptor.loadRemoteData();
        }
    }
});







